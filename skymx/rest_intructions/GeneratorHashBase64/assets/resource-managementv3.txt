<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:toatech:ResourceManagement:1.0">
   <soapenv:Header/>
   <soapenv:Body>
      <urn:get_resources_zones>
         <!--You may enter the following 2 items in any order-->
           <user>
            <now>2018-04-05T17:56:56.219Z</now>
            <login>SOAP</login>
            <company>sky-mx2.test</company>
            <auth_string>95179780953811875d3aa341d43ff78b69c1b7299caa070f5a496238dafb727c</auth_string>
         </user>

         <resources>
            <!--1 or more repetitions:-->
            <resource>
               <!--You may enter the following 5 items in any order-->
               <resource_id>401000009180</resource_id>
               <!--Optional:-->
               <date>2018-04-05</date>
               <!--Optional:-->
               <include_children>immediate</include_children>
               <!--Optional:-->
               <include_inactive>1</include_inactive>
               <!--Optional:-->
               <duration></duration>
            </resource>
         </resources>
      </urn:get_resources_zones>
   </soapenv:Body>
</soapenv:Envelope>