package cl.inmotion.ofsc.model;

public class User {
	
	private String login;
	private String userType;
	private String status;
	private String language;
	private String timezone;
	private String uname;
	private String sutime_fid;
	private String sudate_fid;
	private String sulong_date_fid;
	private String password;
	private String password_temporary;
	private String login_policy;
	private String main_resource_id;
	private String calendarName;
	

	public String getCalendarName() {
		return calendarName;
	}

	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}

	public String getUname() {
		return uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getSutime_fid() {
		return sutime_fid;
	}

	public void setSutime_fid(String sutime_fid) {
		this.sutime_fid = sutime_fid;
	}

	public String getSudate_fid() {
		return sudate_fid;
	}

	public void setSudate_fid(String sudate_fid) {
		this.sudate_fid = sudate_fid;
	}

	public String getSulong_date_fid() {
		return sulong_date_fid;
	}

	public void setSulong_date_fid(String sulong_date_fid) {
		this.sulong_date_fid = sulong_date_fid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword_temporary() {
		return password_temporary;
	}

	public void setPassword_temporary(String password_temporary) {
		this.password_temporary = password_temporary;
	}
	
	public String getLogin_policy() {
		return login_policy;
	}

	public void setLogin_policy(String login_policy) {
		this.login_policy = login_policy;
	}
	
	public String getMain_resource_id() {
		return main_resource_id;
	}

	public void setMain_resource_id(String main_resource_id) {
		this.main_resource_id = main_resource_id;
	}

	public User() {	
	}
	
	public User(
			String login, //26//ulogin
			String userType, //27//user_type_id
			String status, //28//sustatus
			String language, //29//ulanguage
			String timezone,//31//su_zid
			String uname,//35//uname
			String sutime_fid,//9//sutime_fid
			String sudate_fid,//10//sudate_fid
			String sulong_date_fid,//XX//sulong_date_fid
			String password,//XX//password
			String password_temporary,//XX//password_temporary
			String main_resource_id,//1//main_resource_id
			String login_policy//1//login_policy
		
			){
		this.login=login;
		this.userType=userType;
		this.status=status;
		this.language=language;
		this.timezone=timezone;
		this.uname=uname;
		this.sutime_fid=sutime_fid;
		this.sudate_fid=sudate_fid;
		//this.sulong_date_fid=sulong_date_fid;
		this.sulong_date_fid="a�o/mes/d�a";
		this.password=password;
		this.password_temporary=password_temporary;
		this.main_resource_id=main_resource_id;
		this.login_policy=login_policy;
		
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	} 
	
	@Override
	public String toString(){
		return "User {login:"+login+" userType:"+userType+" status:"+status+" language:"+language+" timezone:"+timezone+"}";
	}
}
