package cl.inmotion.ofsc.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import cl.inmotion.ofsc.model.Region;
import cl.inmotion.ofsc.model.Resource;
import cl.inmotion.ofsc.model.User;
import cl.inmotion.ofsc.model.WorkZone;
//import cl.inmotion.ofsc.servicios.ResourceManagement;
import org.apache.log4j.Logger;

public class UtilCSV {

	final static Logger logger = Logger.getLogger(UtilCSV.class);

	public HashMap<String, List<String>> run() {
		HashMap<String, List<String>> map = new HashMap<String, List<String>>();
		String csvFile = "/home/jboss/sky/work_zones_sky.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";

		try {
			//
			br = new BufferedReader(new FileReader(csvFile));
			while ((line = br.readLine()) != null) {
				line = line.replace(" ", "_");
				logger.warn(line);

				String[] l = line.split(cvsSplitBy);
				// System.out.println("WorkZone=" + l[1] + " , Key:"+l[2]);
				List<String> keys = (map.get(l[1]) == null ? new ArrayList<String>() : map.get(l[1]));
				keys.add(removeTilde(l[2]));
				map.put(removeTilde(l[1]), keys);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return map;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Region> CSVToRegion(HashMap<String, List<String>> map) {
		List<Region> result = new ArrayList<Region>();
		Iterator it = map.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry e = (Map.Entry) it.next();
			// System.out.println(e.getKey() + " " + e.getValue());
			String key = (String) e.getKey();
			String[] aux = key.split("-");
			String r = aux[0];

			Region reg = hasRegion(result, r);
			if (reg == null) {
				Region newRegion = new Region();
				List<WorkZone> newWorkZones = new ArrayList<WorkZone>();
				WorkZone wz = new WorkZone();

				wz.setName(key);
				wz.setKeys((List<String>) e.getValue());
				newWorkZones.add(wz);
				newRegion.setName(r.toUpperCase());
				newRegion.setWorkzones(newWorkZones);
				result.add(newRegion);
			} else {
				WorkZone wz = new WorkZone();
				wz.setName(key);
				wz.setKeys((List<String>) e.getValue());
				List<WorkZone> lr = reg.getWorkzones();
				lr.add(wz);
				reg.setWorkzones(lr);
			}
		}
		return result;
	}

	public List<Resource> CSVToResource() {
		List<Resource> result = new ArrayList<Resource>();
		String csvFile = "C:\\Users\\inmotion\\Documents\\z\\resourcessky\\treetest.csv";
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ";";

		try {
			//
			br = new BufferedReader(new FileReader(csvFile));

			while ((line = br.readLine()) != null) {
				try {

					System.out.println(line);
					String[] l = line.split(cvsSplitBy);
					logger.warn(line);
					User u = new User();
					u.setLogin(l[26]);
					u.setUserType(l[27]);
					u.setStatus(l[28]);
					u.setLanguage(l[29]);
					// u.setPassword(l[26]);
					u.setUname(l[35]);
					u.setTimezone(l[8]);
					u.setSutime_fid(l[9]);
					u.setSudate_fid(l[10]);
					u.setSulong_date_fid("a�o/mes/d�a d�a de la semana");
					u.setPassword(l[33]);
					u.setPassword_temporary("yes");
					u.setMain_resource_id(l[1]);
					u.setLogin_policy(l[30]);
					u.setCalendarName(l[19]);
				  
					Resource r = new Resource(l[0], // 0//parent_id
							l[1], // 1//external_id
							l[2], // 2//pname
							l[3], // 3//ptype
							l[4], // 4//pactive
							l[5], // 5//planguage
							l[6], // 6//email
							l[7], // 7//pphone
							l[8], // 8//time_zone
							// String calendar, //X//XXX //TODO deprecated
							l[15], l[9], // 9//time_format
							l[10], // 10//date_format
							l[25], // 25//resource_workzones
							l[0], // 0//XR_PartnerID
							l[3], // 3//pcapacity_bucket//TODO tiene que tener la logica
							l[17], // 17//work_skills
							l[34], // 34//capacity_categories//TODO tiene que tener la logica
							(l.length > 36) ? l[36] : "", // 36//XR_MasterID
							u);
					r.setPcapacity_bucket("yes");

					try {
						r.setXR_RPID(l[37]);
					} catch (IndexOutOfBoundsException e) {
						r.setXR_RPID("");
					}

					// r.setOrganization_id(l[11]);
					result.add(r);
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return result;
	}

	private Region hasRegion(List<Region> list, String region) {
		if (list == null || list.size() < 1)
			return null;
		for (Region r : list) {
			if (r.getName().compareToIgnoreCase(region) == 0)
				return r;
		}
		return null;
	}

	private String removeTilde(String s) {
		if (s == null)
			return null;
		return s.replace("�", "a").replace("�", "e").replace("�", "i").replace("�", "o").replace("�", "u");
	}
}
