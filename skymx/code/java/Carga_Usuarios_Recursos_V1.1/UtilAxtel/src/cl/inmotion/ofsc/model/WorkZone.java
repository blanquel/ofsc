package cl.inmotion.ofsc.model;

import java.util.List;

public class WorkZone {
	
	private String name;
	
	private List<String> keys;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getKeys() {
		return keys;
	}

	public void setKeys(List<String> keys) {
		this.keys = keys;
	}	
	public String getKeysJSON(){
		String json="";
		if(keys.size()==0)
			return json;
		for(String k:keys)
			json+="\""+k+"\",";
		return json.substring(0, json.length()-1);
			
	}
	public String getWorkCenter(){
		if(name.compareTo("")==0)
			return "";
		String[] aux=name.split("-");
		if(aux.length<2)
			return name;
		else
			return aux[1];					
	}

}
