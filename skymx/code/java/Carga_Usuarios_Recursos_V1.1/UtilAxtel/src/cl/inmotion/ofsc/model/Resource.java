package cl.inmotion.ofsc.model;

public class Resource {
	
	private String parentId;	
	private String externalId;	
	private String name;	
	private String resourceType;	
	private String resourceStatus;
	private String language;
	private String email;
	private String phone;
	private String timezone;
	private String calendar;
	private String time_format;
	private String date_format;
	private String resource_workzones;
	private String XR_PartnerID;
	private String XR_MasterID;
	private String pcapacity_bucket;
	private String work_skills;
//	private String p_rprid;
	private String capacity_categories;
//	private String resource_time_slots;
	private User user;
    private String XR_RPID;
//    private String organization_id ;
//    
//	
//	public String getOrganization_id() {
//		return organization_id;
//	}
//
//	public void setOrganization_id(String organization_id) {
//		this.organization_id = organization_id;
//	}

	public String getXR_RPID() {
		return XR_RPID;
	}

	public void setXR_RPID(String xR_RPID) {
		XR_RPID = xR_RPID;
	}
	


	public String getPcapacity_bucket() {
		return pcapacity_bucket;
	}

	public void setPcapacity_bucket(String pcapacity_bucket) {
		this.pcapacity_bucket = pcapacity_bucket;
	}
	public String getCapacity_categories() {
		return capacity_categories;
	}

	public void setCapacity_categories(String capacity_categories) {
		this.capacity_categories = capacity_categories;
	}

	public String getWork_skills() {
		return work_skills;
	}

	public void setWork_skills(String work_skills) {
		this.work_skills = work_skills;
	}

	public String getTime_format() {
		return time_format;
	}

	public void setTime_format(String time_format) {
		this.time_format = time_format;
	}

	public String getDate_format() {
		return date_format;
	}

	public void setDate_format(String date_format) {
		this.date_format = date_format;
	}

	public String getResource_workzones() {
		return resource_workzones;
	}

	public void setResource_workzones(String resource_workzones) {
		this.resource_workzones = resource_workzones;
	}

	public String getXR_PartnerID() {
		return XR_PartnerID;
	}

	public void setXR_PartnerID(String xR_PartnerID) {
		XR_PartnerID = xR_PartnerID;
	}
	
	public String getXR_MasterID() {
		return XR_MasterID;
	}

	public void setXR_MasterID(String xR_PartnerID) {
		XR_MasterID = xR_PartnerID;
	}
	
//	public String getP_rprid() {
//		return p_rprid;
//	}
//
//	public void setP_rprid(String P_rprid) {
//		p_rprid = P_rprid;
//	}
//

 
//	
//	public String getResource_time_slots() {
//		return resource_time_slots;
//	}
//
//	public void setResource_time_slots(String Presource_time_slots) {
//		resource_time_slots = Presource_time_slots;
//	}		
	
	public Resource() {}
	public Resource( 
			String parentId,//0//parent_id
			String externalId,//1//external_id 
			String name, //2//pname 
			String resourceType, //3//ptype
			String resourceStatus, //4//pactive 
			String language, //5//planguage
			String email, //6//email 
			String phone, //7//pphone 
			String timezone,//8//time_zone 
			String calendar, //X//XXX //TODO deprecated
			String time_format,//9//time_format
			String date_format,//10//date_format
			String resource_workzones,//25//resource_workzones
			String XR_PartnerID,//0//XR_PartnerID
			String pcapacity_bucket,//3//pcapacity_bucket//TODO tiene que tener la logica
			String work_skills,//17//work_skills//TODO tiene que tener la logica
			String capacity_categories,//34//capacity_categories//TODO tiene que tener la logica
			String XR_MasterID,//0//XR_PartnerID
			//String resource_time_slots,//0//resource_time_slots
			//String p_rprid,//X//plan de ruteo
			User user){
		this.parentId=parentId;
		this.externalId=externalId;
		this.name=name;
		this.resourceType=resourceType;
		this.resourceStatus=resourceStatus;
		this.language=language;
		this.email=email;
		this.phone=phone;
		this.timezone=timezone;
		//this.calendar=calendar;
		this.time_format =time_format;
		this.date_format = date_format;
		this.resource_workzones = resource_workzones;
		this.XR_PartnerID = XR_PartnerID;
		this.XR_MasterID = XR_MasterID;
		
//		String pcapacity_bucket_var = "no";
//		String capacity_categories_var = "";
		
//		String resource_time_slots_var = "";
		
//		if(pcapacity_bucket != null 
//				&& !pcapacity_bucket.contentEquals("")
//				&& pcapacity_bucket.contentEquals("DISP")){
//			pcapacity_bucket_var = "yes";
//			capacity_categories_var = capacity_categories;
//			resource_time_slots_var = "";			
//		}
		
//		String p_rprid_var = "Plan Enrutamiento BlueT";
//		this.p_rprid = p_rprid_var;
//		this.pcapacity_bucket = pcapacity_bucket_var;		
		this.work_skills = work_skills;
//		this.capacity_categories = capacity_categories_var;		
		this.user = user;
//		this.resource_time_slots = resource_time_slots_var;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceStatus() {
		return resourceStatus;
	}

	public void setResourceStatus(String resourceStatus) {
		this.resourceStatus = resourceStatus;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getCalendar() {
		return calendar;
	}

	public void setCalendar(String calendar) {
		this.calendar = calendar;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	public String toString(){
		return "Resource { parentId:"+parentId+" externalId:"+externalId+" name:"+name+" resourceType:"+resourceType+
				" resourceStatus:"+resourceStatus+" language:"+language+" email:"+email+" phone:"+phone+" timezone:"+timezone+
				" calendar:"+calendar +" user:"+user.toString()+"}";
	}	
}
