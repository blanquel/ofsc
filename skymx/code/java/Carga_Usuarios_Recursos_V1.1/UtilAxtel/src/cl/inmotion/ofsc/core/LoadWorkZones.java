package cl.inmotion.ofsc.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cl.inmotion.ofsc.model.Region;
import cl.inmotion.ofsc.model.WorkZone;
import cl.inmotion.ofsc.util.UtilCSV;

public class LoadWorkZones {	
	
	private static final String COMMAND="/home/jboss/sky/test.sh";

	public static void main(String[] args) {

		UtilCSV obj = new UtilCSV();
		HashMap<String,List<String>> wz=obj.run();
		List<Region> regiones=obj.CSVToRegion(wz);
		int nWorkZones=0;
		for(Region r:regiones){
			for(WorkZone wzr:r.getWorkzones()){
				/*
				 * '{"workZoneLabel": "RESTZONE2", "status": "active", "travelArea":"company_area", "keys":["key_1","key_2"], "shapes":["SHAPE1A","SHAPE2b"]}'
				 * 
				 * */
				String json="{\"workZoneLabel\":\""+wzr.getName()+"\",\"status\":\"active\",\"travelArea\":\""+r.getName()+"\",\"keys\":["+wzr.getKeysJSON()+"]}";
				//System.out.println("json: "+json);
				String com=COMMAND+" "+json;
				//System.out.println(com);
				String out=executeCommand(com);
				writeLog(out);
				nWorkZones++;
			}				
		}
		System.out.println(nWorkZones);
	}
	
	private static String executeCommand(String command) {

		StringBuffer output = new StringBuffer();

		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = 
                            new BufferedReader(new InputStreamReader(p.getInputStream()));

                        String line = "";			
			while ((line = reader.readLine())!= null) {
				output.append(line + "\n");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return output.toString();

	}
	
	private static void writeLog(String data){
		try{    		
    		File file =new File("output.txt");
    		
    		//if file doesnt exists, then create it
    		if(!file.exists()){
    			file.createNewFile();
    		}
    		//date
    		String pattern = "[yyyy-MM-dd HH:mm:ss]";
    		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    		String date = simpleDateFormat.format(new Date());
    		
    		//true = append file
    		FileWriter fileWritter = new FileWriter(file.getName(),true);
    		BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
    	    bufferWritter.write(date+" "+data);
    	    bufferWritter.close();
    	}catch(IOException e){
    		e.printStackTrace();
    	}
	}
}
