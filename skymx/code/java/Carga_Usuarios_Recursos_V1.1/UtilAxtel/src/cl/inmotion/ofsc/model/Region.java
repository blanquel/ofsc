package cl.inmotion.ofsc.model;

import java.util.List;

public class Region {
	
	private String name;
	
	private List<WorkZone> workzones;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<WorkZone> getWorkzones() {
		return workzones;
	}

	public void setWorkzones(List<WorkZone> workzones) {
		this.workzones = workzones;
	}

}
