package cl.inmotion.ofsc.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import _0._1.ResourceManagement.toatech.AuthNodeElement;

public class ServicioUtil {	
	
	public static final String MD5="MD5";
	
	public static final String SHA256="SHA-256";
	
	private String company;
	
	private String login;
	
	private String password;
	
	public ServicioUtil(){

	}
	
	public ServicioUtil(String company, String login, String password){
		this.company=company;
		this.login=login;
		this.password=password; 
	}
	
	public AuthNodeElement getAuthNodeElement(String algorithm){
		AuthNodeElement user=new AuthNodeElement();
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
		df.setTimeZone(tz);
		String now = df.format(new Date());
		System.out.println(now);		    
		    
		/*
		 * 
		 * auth_string = md5(now+md5(password))
		 * 
		 * auth_string=SHA256(now+SHA256(password+SHA256(login)));
		 * 
		* */
		String auth_string="";
		if(algorithm.compareTo(MD5)==0)
		 	auth_string=this.encrypt(now+this.encrypt(password, algorithm), algorithm);
		else if(algorithm.compareTo(SHA256)==0)
		  	auth_string=this.encrypt(now+this.encrypt(password+this.encrypt(login,algorithm),algorithm),algorithm);		    
		System.out.println(auth_string);
		/*autenticacion*/
		user.setCompany(company);		    
		user.setLogin(login);
		System.out.println(now);
		user.setNow(now); 
		System.out.println(auth_string);
		user.setAuth_string(auth_string);
	    return user;
	}
	
	public String encryptMD5(String input){
		try {
			MessageDigest md=MessageDigest.getInstance("MD5");
			md.update(input.getBytes());
			byte[] digest=md.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			// Now we need to zero pad it if you actually want the full 32 chars.
			while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	public String encryptSHA256(String input){
		try {
			MessageDigest md=MessageDigest.getInstance("SHA-256");
			md.update(input.getBytes());
			byte[] digest=md.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			// Now we need to zero pad it if you actually want the full 32 chars.
			while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	public String encrypt(String input,String algorithm){
		try {
			MessageDigest md=MessageDigest.getInstance(algorithm);
			md.update(input.getBytes());
			byte[] digest=md.digest();
			BigInteger bigInt = new BigInteger(1,digest);
			String hashtext = bigInt.toString(16);
			// Now we need to zero pad it if you actually want the full 32 chars.
			while(hashtext.length() < 32 ){
			  hashtext = "0"+hashtext;
			}
			return hashtext;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}	
	}
	

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
