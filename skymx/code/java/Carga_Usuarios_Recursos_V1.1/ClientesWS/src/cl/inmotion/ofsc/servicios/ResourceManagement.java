package cl.inmotion.ofsc.servicios;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import javax.activation.DataHandler;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

import _0._1.ResourceManagement.toatech.AuthNodeElement;
import _0._1.ResourceManagement.toatech.CalendarElement;
import _0._1.ResourceManagement.toatech.InsertResourceElement;
import _0._1.ResourceManagement.toatech.InsertUserElement;
import _0._1.ResourceManagement.toatech.PropertyElement;
import _0._1.ResourceManagement.toatech.ResourceManagementPort;
import _0._1.ResourceManagement.toatech.ResourceManagementServiceLocator;
import _0._1.ResourceManagement.toatech.SetResourceResponseElement;
import _0._1.ResourceManagement.toatech.SetResourcesCalendarsElement;
import _0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement;
import _0._1.ResourceManagement.toatech.SetResourcesZonesElement;
import _0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement;
import _0._1.ResourceManagement.toatech.SetUserResponseElement;
import _0._1.ResourceManagement.toatech.TextPropertyElement;
import _0._1.ResourceManagement.toatech.UpdateResourceElement;
import _0._1.ResourceManagement.toatech.UpdateUserElement;
import _0._1.ResourceManagement.toatech.WorkSkillElement;
import _0._1.ResourceManagement.toatech.WorkSkillList;
import _0._1.ResourceManagement.toatech.ZoneElement;
import cl.inmotion.ofsc.model.Resource;
import cl.inmotion.ofsc.model.User;
import cl.inmotion.ofsc.util.ServicioUtil;
import cl.inmotion.ofsc.util.UtilCSV;

public class ResourceManagement {

	final static Logger logger = Logger.getLogger(ResourceManagement.class);
	private static String currentDate;
	public static String[] ciudades = { "Aguascalientes", "Alvarado", "Canc�n", "Celaya", "Chihuahua", "Ciudad Juarez",
			"Ciudad Victoria", "Coatzacoalcos", "Cuernavaca", "Culiacan", "Durango", "General Zuazua", "Hermosillo",
			"Irapuato", "Leon", "Linares", "Matamoros", "Mazatlan", "Merida", "Mexicali", "Minatitlan", "Morelia",
			"Nuevo Laredo", "Pachuca", "Parras", "Penjamo", "Puebla", "Queretaro", "Reynosa", "Saltillo",
			"San Francisco Del Rincon", "San Juan del Rio", "San Luis Potosi", "Silao", "Tampico", "Texcoco", "Tijuana",
			"Toluca", "Torreon", "Veracruz", "Villahermosa", "Xalapa", "Zacatecas" };

	public static AuthNodeElement getAuthElement() {

		// ServicioUtil util = new ServicioUtil("sky-mx1.test", "SOAP", "Sky_TOA_2018.");
		ServicioUtil util = new ServicioUtil("sky-mx2.test", "SOAP", "SOAP");
		AuthNodeElement user = util.getAuthNodeElement(ServicioUtil.SHA256);
		return user;
	}

	public static String getCurrentDate() {
		return currentDate;
	}

	public static void setCurrentDate(String currentDate) {
		ResourceManagement.currentDate = currentDate;
	}

	public static void main(String[] args) {
		setCurrentDate(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

		UtilCSV obj = new UtilCSV();
		List<Resource> list = obj.CSVToResource();
		System.out.println(" count :" + list.size());
		try {

			for (Resource r : list) {
				try {
					AuthNodeElement user = getAuthElement();

					ResourceManagementServiceLocator service = new ResourceManagementServiceLocator();
					ResourceManagementPort port = service.getResourceManagementPort();

					String resourceName = r.getName();
					System.out.println("resource name:" + resourceName);
					logger.warn("resource name:" + resourceName);
					String id = removeTilde(resourceName.replace(" ", "_")).toUpperCase();

					// int element = r.getResourceType().equals("DISP") ? 14 : 13;
					int element = 13;
					PropertyElement[] props = new PropertyElement[element];
					// List<PropertyElement> props = new ArrayList<PropertyElement>();
					logger.warn("resource name:" + resourceName);

					PropertyElement parent = new PropertyElement();
					parent.setName("parent_id");
					parent.setValue(r.getParentId());
					props[0] = parent;

					PropertyElement type = new PropertyElement();
					type.setName("type");
					type.setValue(r.getResourceType());
					props[1] = type;

					PropertyElement name = new PropertyElement();
					name.setName("name");
					name.setValue(resourceName);
					props[2] = name;

					PropertyElement status = new PropertyElement();
					status.setName("status");
					status.setValue(r.getResourceStatus());
					props[3] = status;

					PropertyElement language = new PropertyElement();
					language.setName("language");
					language.setValue(r.getLanguage());
					props[4] = language;

					PropertyElement time_zone = new PropertyElement();
					time_zone.setName("time_zone");
					time_zone.setValue(r.getTimezone());
					props[5] = time_zone;

					PropertyElement email = new PropertyElement();
					email.setName("email");
					email.setValue(r.getEmail());
					props[6] = email;

					PropertyElement phone = new PropertyElement();
					phone.setName("phone");
					phone.setValue(r.getPhone());
					props[7] = phone;

					PropertyElement time_format = new PropertyElement();
					time_format.setName("time_format");
					time_format.setValue(r.getTime_format());
					props[8] = time_format;
					//
					PropertyElement date_format = new PropertyElement();
					date_format.setName("date_format");
					date_format.setValue(r.getDate_format());
					props[9] = date_format;
					//
					PropertyElement XR_PartnerID = new PropertyElement();
					XR_PartnerID.setName("XR_PartnerID");
					XR_PartnerID.setValue(r.getXR_PartnerID());
					props[10] = XR_PartnerID;
					//
					PropertyElement XR_MasterID = new PropertyElement();
					XR_MasterID.setName("XR_MasterID");
					XR_MasterID.setValue(r.getXR_MasterID());
					props[11] = XR_MasterID;
					//
					PropertyElement XR_RPID = new PropertyElement();
					XR_RPID.setName("XR_RPID");
					XR_RPID.setValue(r.getXR_RPID());
					props[12] = XR_RPID;

					// no hace el cambio por soap unicamente por rest
					/*
					 
					  if (r.getResourceType().equals("DISP")) { PropertyElement organization_id =
					  new PropertyElement(); organization_id.setName("organization_id");
					 
					 * organization_id.setValue(r.getOrganization_id()); props[13] =
					 * organization_id; }
					 */

					// pcapacity_bucket;
					// if (r.getResourceType().equals("DISP_I")) {
					// pcapacity_bucket = new PropertyElement();
					// pcapacity_bucket.setName("pcapacity_bucket");
					// pcapacity_bucket.setValue("yes");
					// props[12] = pcapacity_bucket;
					// } else {
					/*
					 * PropertyElement pcapacity_bucket = new PropertyElement();
					 * pcapacity_bucket.setName("pcapacity_bucket");
					 * pcapacity_bucket.setValue("yes"); props[12] = pcapacity_bucket;
					 */
					// }

					// PropertyElement p_rprid=new PropertyElement();
					// p_rprid.setName("p_rprid");
					// p_rprid.setValue(r.getP_rprid());
					// props[12]=p_rprid;
					//
					// PropertyElement pcapacity_bucket=new PropertyElement();
					// pcapacity_bucket.setName("pcapacity_bucket");
					// pcapacity_bucket.setValue(r.getPcapacity_bucket());
					// props[13]=pcapacity_bucket;
					//
					// PropertyElement resource_time_slots=new PropertyElement();
					// resource_time_slots.setName("resource_time_slots");
					// resource_time_slots.setValue(r.getResource_time_slots());
					// props[14]=resource_time_slots;

					// TODO tratar las workzones String resource_workzones,//25//resource_workzones

					InsertResourceElement resourceElement = new InsertResourceElement();
					resourceElement.setId(r.getExternalId());

					resourceElement.setProperties(props);

					// Timeslots//En Duro
					// Categoria de Capacidad//En Duro

					WorkSkillList workskills = null;

					if (r.getWork_skills() != null && !r.getWork_skills().equalsIgnoreCase("")) {

						workskills = new WorkSkillList();

						String[] wsk = r.getWork_skills().split("\\|");

						// get work skill
						
						// get work skill
						
						List<String> wskResult = new ArrayList<String>();
						for (int i = 0; i < wsk.length; i++) {
							if (!isNullOrBlank(wsk[i])) {
								wskResult.add(wsk[i]);
							}
						}
						// WorkSkillElement[] wse = new WorkSkillElement[wsk.length];
						WorkSkillElement[] wse = new WorkSkillElement[wskResult.size()];

						int count = 0;

						for (String ws_id : wskResult) {
							wse[count] = new WorkSkillElement(ws_id, 100);
							count++;
						}

						workskills.setWorkskill(wse);
					}

					if (workskills != null) {
						resourceElement.setWorkskills(workskills);
					}
					// user = getAuthElement();
					resourceElement.setUser(user);
					System.out.println("---------resource------");
					logger.warn("\"---------resource------\"");

					SetResourceResponseElement response = port.insert_resource(resourceElement);
					
					if (response.getResult_code() == 34) {
						System.out.println("UPDATE");
						logger.warn("UPDATE");
						UpdateResourceElement ure = new UpdateResourceElement();
						ure.setId(r.getExternalId());
						ure.setProperties(props);

						if (workskills != null) {
							ure.setWorkskills(workskills);
						}
						// user = getAuthElement();
						ure.setUser(user);

						response = port.update_resource(ure);

					}
					System.out.println(response.getResult_code());
					System.out.println(response.getError_msg());
					System.out.println("-----------------");
					logger.warn(response.getResult_code());
					logger.warn(response.getError_msg());
					logger.warn("-----------------");

					User u = r.getUser();

					int cant = 11;
					// TODO
					// REGION START
					// 
					if (u.getUserType() != null && !u.getUserType().equalsIgnoreCase("")
							&& u.getUserType().equalsIgnoreCase("TECN")) {

						cant = 12;
					}
					// REGION END
					PropertyElement[] proper = new PropertyElement[cant];

					PropertyElement pass = new PropertyElement();
					pass.setName("password");
					pass.setValue(new ServicioUtil().encryptMD5(u.getPassword()));
					// encryptMD5(u.getPassword()));
					proper[0] = pass;

					PropertyElement typeU = new PropertyElement();
					typeU.setName("type");
					typeU.setValue(u.getUserType());
					proper[1] = typeU;

					PropertyElement nameU = new PropertyElement();
					nameU.setName("name");
					nameU.setValue(r.getName());
					proper[2] = nameU;

					PropertyElement statusU = new PropertyElement();
					statusU.setName("status");
					statusU.setValue(r.getResourceStatus());
					proper[3] = statusU;

					PropertyElement languageU = new PropertyElement();
					languageU.setName("language");
					languageU.setValue(u.getLanguage());
					proper[4] = languageU;

					PropertyElement time_zoneU = new PropertyElement();
					time_zoneU.setName("time_zone");
					time_zoneU.setValue(u.getTimezone());
					proper[5] = time_zoneU;

					PropertyElement sutime_fid = new PropertyElement();
					sutime_fid.setName("time_format");
					sutime_fid.setValue(u.getSutime_fid());
					proper[6] = sutime_fid;

					PropertyElement sudate_fid = new PropertyElement();
					sudate_fid.setName("date_format");
					sudate_fid.setValue(u.getSudate_fid());
					proper[7] = sudate_fid;

					PropertyElement sulong_date_fid = new PropertyElement();
					sulong_date_fid.setName("long_date_format");
					sulong_date_fid.setValue(u.getSulong_date_fid());
					proper[8] = sulong_date_fid;

					PropertyElement password_temporary = new PropertyElement();
					password_temporary.setName("password_temporary");
					password_temporary.setValue(u.getPassword_temporary());
					proper[9] = password_temporary;

					// PropertyElement login_blocked_to=new PropertyElement();
					// login_blocked_to.setName("login_blocked_to");
					// login_blocked_to.setValue("2018-01-01 10:10:00 (GMT-6)");
					// proper[10]=login_blocked_to;

					PropertyElement login_policy = new PropertyElement();
					login_policy.setName("login_policy");
					login_policy.setValue(u.getLogin_policy());
					proper[10] = login_policy;

					// TODO
					// REGION START
					// Para que sirve este validaci�n
					
					if (u.getUserType() != null && !u.getUserType().equalsIgnoreCase("")
							&& u.getUserType().equalsIgnoreCase("TECN")) {
						PropertyElement main_resource_id = new PropertyElement();
						main_resource_id.setName("main_resource_id");
						main_resource_id.setValue(r.getExternalId());
						proper[11] = main_resource_id;
					}
					// REGION END

					// System.out.println("login:"+u.getLogin()+"
					// passMD5:"+util.encryptMD5(u.getLogin()));
					String[] resources = new String[1];
					resources[0] = r.getExternalId();

					InsertUserElement userElement = new InsertUserElement();
					userElement.setLogin(u.getLogin());
					userElement.setProperties(proper);
					// user = getAuthElement();
					userElement.setUser(user);
					userElement.setResources(resources);
					System.out.println("-----------user---------");
					SetUserResponseElement responseU = port.insert_user(userElement);
					System.out.println(responseU.getResult_code());
					System.out.println(responseU.getError_msg());

					logger.warn(response.getResult_code());
					logger.warn(response.getError_msg());
					logger.warn("-----------------");

					if (responseU.getResult_code() == 42) {
						System.out.println("UPDATE");
						UpdateUserElement uue = new UpdateUserElement();
						uue.setLogin(u.getLogin());
						uue.setProperties(proper);
						// user = getAuthElement();
						uue.setUser(user);
						uue.setResources(resources);
						System.out.println("-----------user---------");
						responseU = port.update_user(uue);
						System.out.println(responseU.getResult_code());
						System.out.println(responseU.getError_msg());
						// logger.info("Prueba");
					}

					System.out.println("-----------------");
					logger.warn("-----------------");
					// break;

					// Cargo el calendario
					loadCalendar(r);

					// Cargo las zonas de trabajo
					// loadZone(r);
					// Thread.sleep(500);
					
				} catch (Exception e) {
					System.out.println(e.getMessage());
					Thread.sleep(4800);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.warn("FIN: ");
		// logger.warn("Result_code: " + response.getResult_code());

	}

	private static String removeTilde(String s) {
		if (s == null)
			return null;
		return s.replace("�", "a").replace("�", "e").replace("�", "i").replace("�", "o").replace("�", "u");
	}

	private static String[] getZones(Resource resource) {
		String workzones = resource.getResource_workzones();
		String workzonesArr[] = workzones.split("\\|");
		String allzones = "";
		for (String zone : workzonesArr) {
			if (zone.equalsIgnoreCase("")) {
				continue;
			}
			String zoneRange[] = zone.split(",");
			String[] zr = zoneRange[1].split("-");
			Integer zD = Integer.parseInt(zr[0]);// zr[0]
			Integer zH = Integer.parseInt(zr[1]);// zr[0]

			String range = "" + zD;
			for (int i = 1; i < (zH - zD); i++) {
				range += "|" + (zD + i);
			}
			range += "|" + (zH);
			// System.out.println(zoneRange[1]+"-"+range);
			allzones += (range + "|");
		}
		allzones = (!allzones.equalsIgnoreCase("")) ? allzones.substring(0, allzones.length() - 1) : "";
		String[] arr = allzones.split("\\|");
		Hashtable<String, String> hash = new Hashtable<String, String>();

		for (int i = 0; i < arr.length; i++) {
			hash.put(arr[i], arr[i]);
		}
		Enumeration<String> emune = hash.elements();
		int count = 0;
		String[] allzonesArr = new String[hash.size()];
		while (emune.hasMoreElements()) {
			String elem = (String) emune.nextElement();
			allzonesArr[count] = elem;
			count++;
		}
		// System.out.println("Cantidad:" + hash.size());
		return allzonesArr;// allzones.split("\\|");
	}

	// private static void loadZone(Resource resource) throws Exception {
	//
	// AuthNodeElement user = getAuthElement();
	//
	// ResourceManagementServiceLocator service = new
	// ResourceManagementServiceLocator();
	// org.apache.axis.client.Stub s = (org.apache.axis.client.Stub)
	// service.getResourceManagementPort();
	// s.setTimeout(9000000);
	// ResourceManagementPort port = service.getResourceManagementPort();
	//
	// SetResourcesZonesElement params = new SetResourcesZonesElement();
	// params.setUser(user);
	//
	// String[] zonesArr = getZones(resource);
	//
	// ZoneElement[] zones = new ZoneElement[zonesArr.length];
	//
	// for (int i = 0; i < zonesArr.length; i++) {
	// zones[i] = new ZoneElement();
	//
	// Date date = new Date();
	// date.getTime();
	// // String dateFormat = date.parse("yyyy-MM-dd");
	//
	// TextPropertyElement[] properties = new TextPropertyElement[5];
	// properties[0] = new TextPropertyElement("date", getCurrentDate());
	// properties[1] = new TextPropertyElement("resource_id",
	// resource.getExternalId());
	// properties[2] = new TextPropertyElement("zone", zonesArr[i]);
	// properties[3] = new TextPropertyElement("ratio", "100");
	// properties[4] = new TextPropertyElement("duration", "365");
	//
	// zones[i].setProperties(properties);
	//
	// }
	//
	// params.setZones(zones);
	// System.out.println("resource_id: " + resource.getExternalId());
	// System.out.println("zones: " + zones.length);
	// logger.warn("resource_id: " + resource.getExternalId());
	// logger.warn("zones: " + zones.length);
	// try {
	// SetResourcesZonesResponseElement response = port.set_resources_zones(params);
	// System.out.println("Error_msg: " + response.getError_msg());
	// System.out.println("Result_code: " + response.getResult_code());
	// logger.warn("Error_msg: " + response.getError_msg());
	// logger.warn("Result_code: " + response.getResult_code());
	//
	// } catch (Exception e) {
	// System.err.println("ERROR: " + e.getLocalizedMessage());
	// logger.error("ERROR: " + e.getLocalizedMessage());
	// e.printStackTrace();
	// }
	// }

	private static void loadCalendar(Resource resource) throws Exception {

		AuthNodeElement user = getAuthElement();

		ResourceManagementServiceLocator service = new ResourceManagementServiceLocator();
		ResourceManagementPort port = service.getResourceManagementPort();

		SetResourcesCalendarsElement params = new SetResourcesCalendarsElement();
		params.setUser(user);
		CalendarElement[] calendars = new CalendarElement[1];

		TextPropertyElement[] properties = new TextPropertyElement[5];
		// properties[0] = new TextPropertyElement("date", "2018-02-12");
		properties[0] = new TextPropertyElement("date", getCurrentDate());
		// Tengo dudas
		properties[1] = new TextPropertyElement("resource_id", resource.getExternalId());
		properties[2] = new TextPropertyElement("duration", "365");
		// properties[3] = new TextPropertyElement("schedule", "PROSE");
		properties[3] = new TextPropertyElement("schedule", resource.getUser().getCalendarName());
		properties[4] = new TextPropertyElement("comment", "Ingresado por API - Duraci�n: 1 a�o");

		calendars[0] = new CalendarElement(properties);

		params.setCalendars(calendars);

		SetResourcesCalendarsResponseElement response = port.set_resources_calendars(params);
		System.out.println("Error_msg: " + response.getError_msg());
		System.out.println("Result_code: " + response.getResult_code());

		logger.warn("Error_msg: " + response.getError_msg());
		logger.warn("Result_code: " + response.getResult_code());
	}

	private static boolean isNullOrBlank(String s) {
		return (s == null || s.trim().equals(""));
	}
}
