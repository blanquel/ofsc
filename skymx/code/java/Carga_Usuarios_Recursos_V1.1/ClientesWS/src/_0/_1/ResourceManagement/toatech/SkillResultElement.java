/**
 * SkillResultElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class SkillResultElement  implements java.io.Serializable {
    private int skill_result_code;

    private java.lang.String skill_error_msg;

    private java.lang.String userdata;

    public SkillResultElement() {
    }

    public SkillResultElement(
           int skill_result_code,
           java.lang.String skill_error_msg,
           java.lang.String userdata) {
           this.skill_result_code = skill_result_code;
           this.skill_error_msg = skill_error_msg;
           this.userdata = userdata;
    }


    /**
     * Gets the skill_result_code value for this SkillResultElement.
     * 
     * @return skill_result_code
     */
    public int getSkill_result_code() {
        return skill_result_code;
    }


    /**
     * Sets the skill_result_code value for this SkillResultElement.
     * 
     * @param skill_result_code
     */
    public void setSkill_result_code(int skill_result_code) {
        this.skill_result_code = skill_result_code;
    }


    /**
     * Gets the skill_error_msg value for this SkillResultElement.
     * 
     * @return skill_error_msg
     */
    public java.lang.String getSkill_error_msg() {
        return skill_error_msg;
    }


    /**
     * Sets the skill_error_msg value for this SkillResultElement.
     * 
     * @param skill_error_msg
     */
    public void setSkill_error_msg(java.lang.String skill_error_msg) {
        this.skill_error_msg = skill_error_msg;
    }


    /**
     * Gets the userdata value for this SkillResultElement.
     * 
     * @return userdata
     */
    public java.lang.String getUserdata() {
        return userdata;
    }


    /**
     * Sets the userdata value for this SkillResultElement.
     * 
     * @param userdata
     */
    public void setUserdata(java.lang.String userdata) {
        this.userdata = userdata;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SkillResultElement)) return false;
        SkillResultElement other = (SkillResultElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.skill_result_code == other.getSkill_result_code() &&
            ((this.skill_error_msg==null && other.getSkill_error_msg()==null) || 
             (this.skill_error_msg!=null &&
              this.skill_error_msg.equals(other.getSkill_error_msg()))) &&
            ((this.userdata==null && other.getUserdata()==null) || 
             (this.userdata!=null &&
              this.userdata.equals(other.getUserdata())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getSkill_result_code();
        if (getSkill_error_msg() != null) {
            _hashCode += getSkill_error_msg().hashCode();
        }
        if (getUserdata() != null) {
            _hashCode += getUserdata().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SkillResultElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SkillResultElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skill_result_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "skill_result_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skill_error_msg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "skill_error_msg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userdata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userdata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
