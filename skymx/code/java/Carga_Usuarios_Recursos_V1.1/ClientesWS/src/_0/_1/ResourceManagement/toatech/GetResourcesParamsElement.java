/**
 * GetResourcesParamsElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class GetResourcesParamsElement  implements java.io.Serializable {
    private java.lang.String resource_id;

    private java.lang.String date;

    private java.lang.String include_children;

    private java.lang.String include_inactive;

    private java.lang.Integer duration;

    public GetResourcesParamsElement() {
    }

    public GetResourcesParamsElement(
           java.lang.String resource_id,
           java.lang.String date,
           java.lang.String include_children,
           java.lang.String include_inactive,
           java.lang.Integer duration) {
           this.resource_id = resource_id;
           this.date = date;
           this.include_children = include_children;
           this.include_inactive = include_inactive;
           this.duration = duration;
    }


    /**
     * Gets the resource_id value for this GetResourcesParamsElement.
     * 
     * @return resource_id
     */
    public java.lang.String getResource_id() {
        return resource_id;
    }


    /**
     * Sets the resource_id value for this GetResourcesParamsElement.
     * 
     * @param resource_id
     */
    public void setResource_id(java.lang.String resource_id) {
        this.resource_id = resource_id;
    }


    /**
     * Gets the date value for this GetResourcesParamsElement.
     * 
     * @return date
     */
    public java.lang.String getDate() {
        return date;
    }


    /**
     * Sets the date value for this GetResourcesParamsElement.
     * 
     * @param date
     */
    public void setDate(java.lang.String date) {
        this.date = date;
    }


    /**
     * Gets the include_children value for this GetResourcesParamsElement.
     * 
     * @return include_children
     */
    public java.lang.String getInclude_children() {
        return include_children;
    }


    /**
     * Sets the include_children value for this GetResourcesParamsElement.
     * 
     * @param include_children
     */
    public void setInclude_children(java.lang.String include_children) {
        this.include_children = include_children;
    }


    /**
     * Gets the include_inactive value for this GetResourcesParamsElement.
     * 
     * @return include_inactive
     */
    public java.lang.String getInclude_inactive() {
        return include_inactive;
    }


    /**
     * Sets the include_inactive value for this GetResourcesParamsElement.
     * 
     * @param include_inactive
     */
    public void setInclude_inactive(java.lang.String include_inactive) {
        this.include_inactive = include_inactive;
    }


    /**
     * Gets the duration value for this GetResourcesParamsElement.
     * 
     * @return duration
     */
    public java.lang.Integer getDuration() {
        return duration;
    }


    /**
     * Sets the duration value for this GetResourcesParamsElement.
     * 
     * @param duration
     */
    public void setDuration(java.lang.Integer duration) {
        this.duration = duration;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetResourcesParamsElement)) return false;
        GetResourcesParamsElement other = (GetResourcesParamsElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.resource_id==null && other.getResource_id()==null) || 
             (this.resource_id!=null &&
              this.resource_id.equals(other.getResource_id()))) &&
            ((this.date==null && other.getDate()==null) || 
             (this.date!=null &&
              this.date.equals(other.getDate()))) &&
            ((this.include_children==null && other.getInclude_children()==null) || 
             (this.include_children!=null &&
              this.include_children.equals(other.getInclude_children()))) &&
            ((this.include_inactive==null && other.getInclude_inactive()==null) || 
             (this.include_inactive!=null &&
              this.include_inactive.equals(other.getInclude_inactive()))) &&
            ((this.duration==null && other.getDuration()==null) || 
             (this.duration!=null &&
              this.duration.equals(other.getDuration())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResource_id() != null) {
            _hashCode += getResource_id().hashCode();
        }
        if (getDate() != null) {
            _hashCode += getDate().hashCode();
        }
        if (getInclude_children() != null) {
            _hashCode += getInclude_children().hashCode();
        }
        if (getInclude_inactive() != null) {
            _hashCode += getInclude_inactive().hashCode();
        }
        if (getDuration() != null) {
            _hashCode += getDuration().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetResourcesParamsElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesParamsElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resource_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resource_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("include_children");
        elemField.setXmlName(new javax.xml.namespace.QName("", "include_children"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("include_inactive");
        elemField.setXmlName(new javax.xml.namespace.QName("", "include_inactive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duration");
        elemField.setXmlName(new javax.xml.namespace.QName("", "duration"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
