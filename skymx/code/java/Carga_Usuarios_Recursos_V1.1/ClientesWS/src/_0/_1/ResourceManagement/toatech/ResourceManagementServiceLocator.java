/**
 * ResourceManagementServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class ResourceManagementServiceLocator extends org.apache.axis.client.Service implements _0._1.ResourceManagement.toatech.ResourceManagementService {

    public ResourceManagementServiceLocator() {
    }


    public ResourceManagementServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ResourceManagementServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ResourceManagementPort
    private java.lang.String ResourceManagementPort_address = "https://api.etadirect.com/soap/resource-management/v3/";

    public java.lang.String getResourceManagementPortAddress() {
        return ResourceManagementPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ResourceManagementPortWSDDServiceName = "ResourceManagementPort";

    public java.lang.String getResourceManagementPortWSDDServiceName() {
        return ResourceManagementPortWSDDServiceName;
    }

    public void setResourceManagementPortWSDDServiceName(java.lang.String name) {
        ResourceManagementPortWSDDServiceName = name;
    }

    public _0._1.ResourceManagement.toatech.ResourceManagementPort getResourceManagementPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ResourceManagementPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getResourceManagementPort(endpoint);
    }

    public _0._1.ResourceManagement.toatech.ResourceManagementPort getResourceManagementPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            _0._1.ResourceManagement.toatech.ResourceManagementBindingStub _stub = new _0._1.ResourceManagement.toatech.ResourceManagementBindingStub(portAddress, this);
            _stub.setPortName(getResourceManagementPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setResourceManagementPortEndpointAddress(java.lang.String address) {
        ResourceManagementPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (_0._1.ResourceManagement.toatech.ResourceManagementPort.class.isAssignableFrom(serviceEndpointInterface)) {
                _0._1.ResourceManagement.toatech.ResourceManagementBindingStub _stub = new _0._1.ResourceManagement.toatech.ResourceManagementBindingStub(new java.net.URL(ResourceManagementPort_address), this);
                _stub.setPortName(getResourceManagementPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ResourceManagementPort".equals(inputPortName)) {
            return getResourceManagementPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceManagementService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceManagementPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ResourceManagementPort".equals(portName)) {
            setResourceManagementPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
