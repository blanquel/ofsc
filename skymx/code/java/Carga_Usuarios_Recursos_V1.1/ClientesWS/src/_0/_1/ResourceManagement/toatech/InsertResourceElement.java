/**
 * InsertResourceElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class InsertResourceElement  implements java.io.Serializable {
    private _0._1.ResourceManagement.toatech.AuthNodeElement user;

    private java.lang.String id;

    private _0._1.ResourceManagement.toatech.PropertyElement[] properties;

    private java.lang.String[] users;

    private _0._1.ResourceManagement.toatech.WorkSkillList workskills;

    public InsertResourceElement() {
    }

    public InsertResourceElement(
           _0._1.ResourceManagement.toatech.AuthNodeElement user,
           java.lang.String id,
           _0._1.ResourceManagement.toatech.PropertyElement[] properties,
           java.lang.String[] users,
           _0._1.ResourceManagement.toatech.WorkSkillList workskills) {
           this.user = user;
           this.id = id;
           this.properties = properties;
           this.users = users;
           this.workskills = workskills;
    }


    /**
     * Gets the user value for this InsertResourceElement.
     * 
     * @return user
     */
    public _0._1.ResourceManagement.toatech.AuthNodeElement getUser() {
        return user;
    }


    /**
     * Sets the user value for this InsertResourceElement.
     * 
     * @param user
     */
    public void setUser(_0._1.ResourceManagement.toatech.AuthNodeElement user) {
        this.user = user;
    }


    /**
     * Gets the id value for this InsertResourceElement.
     * 
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }


    /**
     * Sets the id value for this InsertResourceElement.
     * 
     * @param id
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }


    /**
     * Gets the properties value for this InsertResourceElement.
     * 
     * @return properties
     */
    public _0._1.ResourceManagement.toatech.PropertyElement[] getProperties() {
        return properties;
    }


    /**
     * Sets the properties value for this InsertResourceElement.
     * 
     * @param properties
     */
    public void setProperties(_0._1.ResourceManagement.toatech.PropertyElement[] properties) {
        this.properties = properties;
    }


    /**
     * Gets the users value for this InsertResourceElement.
     * 
     * @return users
     */
    public java.lang.String[] getUsers() {
        return users;
    }


    /**
     * Sets the users value for this InsertResourceElement.
     * 
     * @param users
     */
    public void setUsers(java.lang.String[] users) {
        this.users = users;
    }


    /**
     * Gets the workskills value for this InsertResourceElement.
     * 
     * @return workskills
     */
    public _0._1.ResourceManagement.toatech.WorkSkillList getWorkskills() {
        return workskills;
    }


    /**
     * Sets the workskills value for this InsertResourceElement.
     * 
     * @param workskills
     */
    public void setWorkskills(_0._1.ResourceManagement.toatech.WorkSkillList workskills) {
        this.workskills = workskills;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InsertResourceElement)) return false;
        InsertResourceElement other = (InsertResourceElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.properties==null && other.getProperties()==null) || 
             (this.properties!=null &&
              java.util.Arrays.equals(this.properties, other.getProperties()))) &&
            ((this.users==null && other.getUsers()==null) || 
             (this.users!=null &&
              java.util.Arrays.equals(this.users, other.getUsers()))) &&
            ((this.workskills==null && other.getWorkskills()==null) || 
             (this.workskills!=null &&
              this.workskills.equals(other.getWorkskills())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getProperties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProperties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProperties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUsers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUsers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUsers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getWorkskills() != null) {
            _hashCode += getWorkskills().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InsertResourceElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "InsertResourceElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "AuthNodeElement"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("properties");
        elemField.setXmlName(new javax.xml.namespace.QName("", "properties"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "PropertyElement"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "property"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("users");
        elemField.setXmlName(new javax.xml.namespace.QName("", "users"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "login"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workskills");
        elemField.setXmlName(new javax.xml.namespace.QName("", "workskills"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "WorkSkillList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
