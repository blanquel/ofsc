/**
 * LocationResultElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class LocationResultElement  implements java.io.Serializable {
    private int result_code;

    private java.lang.String result_message;

    public LocationResultElement() {
    }

    public LocationResultElement(
           int result_code,
           java.lang.String result_message) {
           this.result_code = result_code;
           this.result_message = result_message;
    }


    /**
     * Gets the result_code value for this LocationResultElement.
     * 
     * @return result_code
     */
    public int getResult_code() {
        return result_code;
    }


    /**
     * Sets the result_code value for this LocationResultElement.
     * 
     * @param result_code
     */
    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }


    /**
     * Gets the result_message value for this LocationResultElement.
     * 
     * @return result_message
     */
    public java.lang.String getResult_message() {
        return result_message;
    }


    /**
     * Sets the result_message value for this LocationResultElement.
     * 
     * @param result_message
     */
    public void setResult_message(java.lang.String result_message) {
        this.result_message = result_message;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocationResultElement)) return false;
        LocationResultElement other = (LocationResultElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.result_code == other.getResult_code() &&
            ((this.result_message==null && other.getResult_message()==null) || 
             (this.result_message!=null &&
              this.result_message.equals(other.getResult_message())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getResult_code();
        if (getResult_message() != null) {
            _hashCode += getResult_message().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocationResultElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationResultElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result_message");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result_message"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
