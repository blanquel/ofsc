/**
 * ResourceListItemElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class ResourceListItemElement  implements java.io.Serializable {
    private _0._1.ResourceManagement.toatech.PropertyElement[] properties;

    private _0._1.ResourceManagement.toatech.WorkSkillList workskills;

    public ResourceListItemElement() {
    }

    public ResourceListItemElement(
           _0._1.ResourceManagement.toatech.PropertyElement[] properties,
           _0._1.ResourceManagement.toatech.WorkSkillList workskills) {
           this.properties = properties;
           this.workskills = workskills;
    }


    /**
     * Gets the properties value for this ResourceListItemElement.
     * 
     * @return properties
     */
    public _0._1.ResourceManagement.toatech.PropertyElement[] getProperties() {
        return properties;
    }


    /**
     * Sets the properties value for this ResourceListItemElement.
     * 
     * @param properties
     */
    public void setProperties(_0._1.ResourceManagement.toatech.PropertyElement[] properties) {
        this.properties = properties;
    }


    /**
     * Gets the workskills value for this ResourceListItemElement.
     * 
     * @return workskills
     */
    public _0._1.ResourceManagement.toatech.WorkSkillList getWorkskills() {
        return workskills;
    }


    /**
     * Sets the workskills value for this ResourceListItemElement.
     * 
     * @param workskills
     */
    public void setWorkskills(_0._1.ResourceManagement.toatech.WorkSkillList workskills) {
        this.workskills = workskills;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResourceListItemElement)) return false;
        ResourceListItemElement other = (ResourceListItemElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.properties==null && other.getProperties()==null) || 
             (this.properties!=null &&
              java.util.Arrays.equals(this.properties, other.getProperties()))) &&
            ((this.workskills==null && other.getWorkskills()==null) || 
             (this.workskills!=null &&
              this.workskills.equals(other.getWorkskills())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getProperties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProperties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProperties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getWorkskills() != null) {
            _hashCode += getWorkskills().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResourceListItemElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceListItemElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("properties");
        elemField.setXmlName(new javax.xml.namespace.QName("", "properties"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "PropertyElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "property"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workskills");
        elemField.setXmlName(new javax.xml.namespace.QName("", "workskills"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "WorkSkillList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
