/**
 * ZoneResultElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class ZoneResultElement  implements java.io.Serializable {
    private int zone_result_code;

    private java.lang.String zone_error_msg;

    private java.lang.String userdata;

    public ZoneResultElement() {
    }

    public ZoneResultElement(
           int zone_result_code,
           java.lang.String zone_error_msg,
           java.lang.String userdata) {
           this.zone_result_code = zone_result_code;
           this.zone_error_msg = zone_error_msg;
           this.userdata = userdata;
    }


    /**
     * Gets the zone_result_code value for this ZoneResultElement.
     * 
     * @return zone_result_code
     */
    public int getZone_result_code() {
        return zone_result_code;
    }


    /**
     * Sets the zone_result_code value for this ZoneResultElement.
     * 
     * @param zone_result_code
     */
    public void setZone_result_code(int zone_result_code) {
        this.zone_result_code = zone_result_code;
    }


    /**
     * Gets the zone_error_msg value for this ZoneResultElement.
     * 
     * @return zone_error_msg
     */
    public java.lang.String getZone_error_msg() {
        return zone_error_msg;
    }


    /**
     * Sets the zone_error_msg value for this ZoneResultElement.
     * 
     * @param zone_error_msg
     */
    public void setZone_error_msg(java.lang.String zone_error_msg) {
        this.zone_error_msg = zone_error_msg;
    }


    /**
     * Gets the userdata value for this ZoneResultElement.
     * 
     * @return userdata
     */
    public java.lang.String getUserdata() {
        return userdata;
    }


    /**
     * Sets the userdata value for this ZoneResultElement.
     * 
     * @param userdata
     */
    public void setUserdata(java.lang.String userdata) {
        this.userdata = userdata;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ZoneResultElement)) return false;
        ZoneResultElement other = (ZoneResultElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.zone_result_code == other.getZone_result_code() &&
            ((this.zone_error_msg==null && other.getZone_error_msg()==null) || 
             (this.zone_error_msg!=null &&
              this.zone_error_msg.equals(other.getZone_error_msg()))) &&
            ((this.userdata==null && other.getUserdata()==null) || 
             (this.userdata!=null &&
              this.userdata.equals(other.getUserdata())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getZone_result_code();
        if (getZone_error_msg() != null) {
            _hashCode += getZone_error_msg().hashCode();
        }
        if (getUserdata() != null) {
            _hashCode += getUserdata().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ZoneResultElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ZoneResultElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone_result_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zone_result_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("zone_error_msg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "zone_error_msg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userdata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userdata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
