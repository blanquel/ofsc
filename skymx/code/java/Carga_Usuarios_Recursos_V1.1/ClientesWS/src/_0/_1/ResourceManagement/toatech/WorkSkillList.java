/**
 * WorkSkillList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class WorkSkillList  implements java.io.Serializable {
    private _0._1.ResourceManagement.toatech.WorkSkillElement[] workskill;

    private java.lang.String[] workskill_group;

    public WorkSkillList() {
    }

    public WorkSkillList(
           _0._1.ResourceManagement.toatech.WorkSkillElement[] workskill,
           java.lang.String[] workskill_group) {
           this.workskill = workskill;
           this.workskill_group = workskill_group;
    }


    /**
     * Gets the workskill value for this WorkSkillList.
     * 
     * @return workskill
     */
    public _0._1.ResourceManagement.toatech.WorkSkillElement[] getWorkskill() {
        return workskill;
    }


    /**
     * Sets the workskill value for this WorkSkillList.
     * 
     * @param workskill
     */
    public void setWorkskill(_0._1.ResourceManagement.toatech.WorkSkillElement[] workskill) {
        this.workskill = workskill;
    }

    public _0._1.ResourceManagement.toatech.WorkSkillElement getWorkskill(int i) {
        return this.workskill[i];
    }

    public void setWorkskill(int i, _0._1.ResourceManagement.toatech.WorkSkillElement _value) {
        this.workskill[i] = _value;
    }


    /**
     * Gets the workskill_group value for this WorkSkillList.
     * 
     * @return workskill_group
     */
    public java.lang.String[] getWorkskill_group() {
        return workskill_group;
    }


    /**
     * Sets the workskill_group value for this WorkSkillList.
     * 
     * @param workskill_group
     */
    public void setWorkskill_group(java.lang.String[] workskill_group) {
        this.workskill_group = workskill_group;
    }

    public java.lang.String getWorkskill_group(int i) {
        return this.workskill_group[i];
    }

    public void setWorkskill_group(int i, java.lang.String _value) {
        this.workskill_group[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WorkSkillList)) return false;
        WorkSkillList other = (WorkSkillList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.workskill==null && other.getWorkskill()==null) || 
             (this.workskill!=null &&
              java.util.Arrays.equals(this.workskill, other.getWorkskill()))) &&
            ((this.workskill_group==null && other.getWorkskill_group()==null) || 
             (this.workskill_group!=null &&
              java.util.Arrays.equals(this.workskill_group, other.getWorkskill_group())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getWorkskill() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWorkskill());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWorkskill(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getWorkskill_group() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWorkskill_group());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWorkskill_group(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WorkSkillList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "WorkSkillList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workskill");
        elemField.setXmlName(new javax.xml.namespace.QName("", "workskill"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "WorkSkillElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workskill_group");
        elemField.setXmlName(new javax.xml.namespace.QName("", "workskill_group"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
