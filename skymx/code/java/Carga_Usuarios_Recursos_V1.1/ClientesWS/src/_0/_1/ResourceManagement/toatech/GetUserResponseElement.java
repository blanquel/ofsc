/**
 * GetUserResponseElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class GetUserResponseElement  implements java.io.Serializable {
    private int result_code;

    private java.lang.String error_msg;

    private _0._1.ResourceManagement.toatech.PropertyElement[] properties;

    private java.lang.String[] resources;

    public GetUserResponseElement() {
    }

    public GetUserResponseElement(
           int result_code,
           java.lang.String error_msg,
           _0._1.ResourceManagement.toatech.PropertyElement[] properties,
           java.lang.String[] resources) {
           this.result_code = result_code;
           this.error_msg = error_msg;
           this.properties = properties;
           this.resources = resources;
    }


    /**
     * Gets the result_code value for this GetUserResponseElement.
     * 
     * @return result_code
     */
    public int getResult_code() {
        return result_code;
    }


    /**
     * Sets the result_code value for this GetUserResponseElement.
     * 
     * @param result_code
     */
    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }


    /**
     * Gets the error_msg value for this GetUserResponseElement.
     * 
     * @return error_msg
     */
    public java.lang.String getError_msg() {
        return error_msg;
    }


    /**
     * Sets the error_msg value for this GetUserResponseElement.
     * 
     * @param error_msg
     */
    public void setError_msg(java.lang.String error_msg) {
        this.error_msg = error_msg;
    }


    /**
     * Gets the properties value for this GetUserResponseElement.
     * 
     * @return properties
     */
    public _0._1.ResourceManagement.toatech.PropertyElement[] getProperties() {
        return properties;
    }


    /**
     * Sets the properties value for this GetUserResponseElement.
     * 
     * @param properties
     */
    public void setProperties(_0._1.ResourceManagement.toatech.PropertyElement[] properties) {
        this.properties = properties;
    }


    /**
     * Gets the resources value for this GetUserResponseElement.
     * 
     * @return resources
     */
    public java.lang.String[] getResources() {
        return resources;
    }


    /**
     * Sets the resources value for this GetUserResponseElement.
     * 
     * @param resources
     */
    public void setResources(java.lang.String[] resources) {
        this.resources = resources;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetUserResponseElement)) return false;
        GetUserResponseElement other = (GetUserResponseElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.result_code == other.getResult_code() &&
            ((this.error_msg==null && other.getError_msg()==null) || 
             (this.error_msg!=null &&
              this.error_msg.equals(other.getError_msg()))) &&
            ((this.properties==null && other.getProperties()==null) || 
             (this.properties!=null &&
              java.util.Arrays.equals(this.properties, other.getProperties()))) &&
            ((this.resources==null && other.getResources()==null) || 
             (this.resources!=null &&
              java.util.Arrays.equals(this.resources, other.getResources())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getResult_code();
        if (getError_msg() != null) {
            _hashCode += getError_msg().hashCode();
        }
        if (getProperties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProperties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProperties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResources() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResources());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResources(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetUserResponseElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUserResponseElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_msg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error_msg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("properties");
        elemField.setXmlName(new javax.xml.namespace.QName("", "properties"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "PropertyElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "property"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resources");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resources"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "id"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
