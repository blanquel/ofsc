package _0._1.ResourceManagement.toatech;

public class ResourceManagementPortProxy implements _0._1.ResourceManagement.toatech.ResourceManagementPort {
  private String _endpoint = null;
  private _0._1.ResourceManagement.toatech.ResourceManagementPort resourceManagementPort = null;
  
  public ResourceManagementPortProxy() {
    _initResourceManagementPortProxy();
  }
  
  public ResourceManagementPortProxy(String endpoint) {
    _endpoint = endpoint;
    _initResourceManagementPortProxy();
  }
  
  private void _initResourceManagementPortProxy() {
    try {
      resourceManagementPort = (new _0._1.ResourceManagement.toatech.ResourceManagementServiceLocator()).getResourceManagementPort();
      if (resourceManagementPort != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)resourceManagementPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)resourceManagementPort)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (resourceManagementPort != null)
      ((javax.xml.rpc.Stub)resourceManagementPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public _0._1.ResourceManagement.toatech.ResourceManagementPort getResourceManagementPort() {
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort;
  }
  
  public _0._1.ResourceManagement.toatech.SetUserResponseElement insert_user(_0._1.ResourceManagement.toatech.InsertUserElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.insert_user(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetUserResponseElement update_user(_0._1.ResourceManagement.toatech.UpdateUserElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.update_user(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetUserResponseElement delete_user(_0._1.ResourceManagement.toatech.DeleteUserElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.delete_user(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetUserResponseElement get_user(_0._1.ResourceManagement.toatech.GetUserElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_user(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetUsersListResponseElement get_users_list(_0._1.ResourceManagement.toatech.GetUsersListElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_users_list(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetResourceResponseElement insert_resource(_0._1.ResourceManagement.toatech.InsertResourceElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.insert_resource(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetResourceResponseElement update_resource(_0._1.ResourceManagement.toatech.UpdateResourceElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.update_resource(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetResourceResponseElement get_resource(_0._1.ResourceManagement.toatech.GetResourceElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_resource(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetResourcesListResponseElement get_resources_list(_0._1.ResourceManagement.toatech.GetResourcesListElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_resources_list(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement set_resources_calendars(_0._1.ResourceManagement.toatech.SetResourcesCalendarsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.set_resources_calendars(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetResourcesCalendarsResponseElement get_resources_calendars(_0._1.ResourceManagement.toatech.GetResourcesCalendarsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_resources_calendars(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetResourcesSkillsResponseElement set_resources_skills(_0._1.ResourceManagement.toatech.SetResourcesSkillsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.set_resources_skills(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetResourcesSkillsResponseElement get_resources_skills(_0._1.ResourceManagement.toatech.GetResourcesSkillsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_resources_skills(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement set_resources_zones(_0._1.ResourceManagement.toatech.SetResourcesZonesElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.set_resources_zones(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetResourcesZonesResponseElement get_resources_zones(_0._1.ResourceManagement.toatech.GetResourcesZonesElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_resources_zones(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetLocationsResponseElement update_locations(_0._1.ResourceManagement.toatech.SetLocationsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.update_locations(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetLocationsResponseElement delete_locations(_0._1.ResourceManagement.toatech.SetLocationsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.delete_locations(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetLocationsResponseElement get_locations(_0._1.ResourceManagement.toatech.GetLocationsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_locations(params);
  }
  
  public _0._1.ResourceManagement.toatech.SetResourcesLocationsResponseElement set_resources_locations(_0._1.ResourceManagement.toatech.SetResourcesLocationsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.set_resources_locations(params);
  }
  
  public _0._1.ResourceManagement.toatech.GetResourcesLocationsResponseElement get_resources_locations(_0._1.ResourceManagement.toatech.GetResourcesLocationsElement params) throws java.rmi.RemoteException{
    if (resourceManagementPort == null)
      _initResourceManagementPortProxy();
    return resourceManagementPort.get_resources_locations(params);
  }
  
  
}