/**
 * CalendarResultElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class CalendarResultElement  implements java.io.Serializable {
    private int calendar_result_code;

    private java.lang.String calendar_error_msg;

    private java.lang.String userdata;

    public CalendarResultElement() {
    }

    public CalendarResultElement(
           int calendar_result_code,
           java.lang.String calendar_error_msg,
           java.lang.String userdata) {
           this.calendar_result_code = calendar_result_code;
           this.calendar_error_msg = calendar_error_msg;
           this.userdata = userdata;
    }


    /**
     * Gets the calendar_result_code value for this CalendarResultElement.
     * 
     * @return calendar_result_code
     */
    public int getCalendar_result_code() {
        return calendar_result_code;
    }


    /**
     * Sets the calendar_result_code value for this CalendarResultElement.
     * 
     * @param calendar_result_code
     */
    public void setCalendar_result_code(int calendar_result_code) {
        this.calendar_result_code = calendar_result_code;
    }


    /**
     * Gets the calendar_error_msg value for this CalendarResultElement.
     * 
     * @return calendar_error_msg
     */
    public java.lang.String getCalendar_error_msg() {
        return calendar_error_msg;
    }


    /**
     * Sets the calendar_error_msg value for this CalendarResultElement.
     * 
     * @param calendar_error_msg
     */
    public void setCalendar_error_msg(java.lang.String calendar_error_msg) {
        this.calendar_error_msg = calendar_error_msg;
    }


    /**
     * Gets the userdata value for this CalendarResultElement.
     * 
     * @return userdata
     */
    public java.lang.String getUserdata() {
        return userdata;
    }


    /**
     * Sets the userdata value for this CalendarResultElement.
     * 
     * @param userdata
     */
    public void setUserdata(java.lang.String userdata) {
        this.userdata = userdata;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CalendarResultElement)) return false;
        CalendarResultElement other = (CalendarResultElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.calendar_result_code == other.getCalendar_result_code() &&
            ((this.calendar_error_msg==null && other.getCalendar_error_msg()==null) || 
             (this.calendar_error_msg!=null &&
              this.calendar_error_msg.equals(other.getCalendar_error_msg()))) &&
            ((this.userdata==null && other.getUserdata()==null) || 
             (this.userdata!=null &&
              this.userdata.equals(other.getUserdata())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getCalendar_result_code();
        if (getCalendar_error_msg() != null) {
            _hashCode += getCalendar_error_msg().hashCode();
        }
        if (getUserdata() != null) {
            _hashCode += getUserdata().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CalendarResultElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "CalendarResultElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calendar_result_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calendar_result_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("calendar_error_msg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "calendar_error_msg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userdata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "userdata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
