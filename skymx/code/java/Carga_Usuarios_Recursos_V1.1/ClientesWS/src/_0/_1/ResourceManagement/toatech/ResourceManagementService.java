/**
 * ResourceManagementService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public interface ResourceManagementService extends javax.xml.rpc.Service {
    public java.lang.String getResourceManagementPortAddress();

    public _0._1.ResourceManagement.toatech.ResourceManagementPort getResourceManagementPort() throws javax.xml.rpc.ServiceException;

    public _0._1.ResourceManagement.toatech.ResourceManagementPort getResourceManagementPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
