/**
 * GetResourcesLocationsResponseElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class GetResourcesLocationsResponseElement  implements java.io.Serializable {
    private int result_code;

    private java.lang.String error_msg;

    private _0._1.ResourceManagement.toatech.ResourceLocationElement[] resource_locations;

    public GetResourcesLocationsResponseElement() {
    }

    public GetResourcesLocationsResponseElement(
           int result_code,
           java.lang.String error_msg,
           _0._1.ResourceManagement.toatech.ResourceLocationElement[] resource_locations) {
           this.result_code = result_code;
           this.error_msg = error_msg;
           this.resource_locations = resource_locations;
    }


    /**
     * Gets the result_code value for this GetResourcesLocationsResponseElement.
     * 
     * @return result_code
     */
    public int getResult_code() {
        return result_code;
    }


    /**
     * Sets the result_code value for this GetResourcesLocationsResponseElement.
     * 
     * @param result_code
     */
    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }


    /**
     * Gets the error_msg value for this GetResourcesLocationsResponseElement.
     * 
     * @return error_msg
     */
    public java.lang.String getError_msg() {
        return error_msg;
    }


    /**
     * Sets the error_msg value for this GetResourcesLocationsResponseElement.
     * 
     * @param error_msg
     */
    public void setError_msg(java.lang.String error_msg) {
        this.error_msg = error_msg;
    }


    /**
     * Gets the resource_locations value for this GetResourcesLocationsResponseElement.
     * 
     * @return resource_locations
     */
    public _0._1.ResourceManagement.toatech.ResourceLocationElement[] getResource_locations() {
        return resource_locations;
    }


    /**
     * Sets the resource_locations value for this GetResourcesLocationsResponseElement.
     * 
     * @param resource_locations
     */
    public void setResource_locations(_0._1.ResourceManagement.toatech.ResourceLocationElement[] resource_locations) {
        this.resource_locations = resource_locations;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetResourcesLocationsResponseElement)) return false;
        GetResourcesLocationsResponseElement other = (GetResourcesLocationsResponseElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.result_code == other.getResult_code() &&
            ((this.error_msg==null && other.getError_msg()==null) || 
             (this.error_msg!=null &&
              this.error_msg.equals(other.getError_msg()))) &&
            ((this.resource_locations==null && other.getResource_locations()==null) || 
             (this.resource_locations!=null &&
              java.util.Arrays.equals(this.resource_locations, other.getResource_locations())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getResult_code();
        if (getError_msg() != null) {
            _hashCode += getError_msg().hashCode();
        }
        if (getResource_locations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResource_locations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResource_locations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetResourcesLocationsResponseElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesLocationsResponseElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_msg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error_msg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resource_locations");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resource_locations"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceLocationElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "resource_location"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
