/**
 * GetResourcesListElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class GetResourcesListElement  implements java.io.Serializable {
    private _0._1.ResourceManagement.toatech.AuthNodeElement user;

    private java.lang.String root_resource_id;

    private java.lang.String include_children;

    private java.lang.Integer select_from;

    private java.lang.Integer select_count;

    private java.lang.String date;

    private java.lang.String[] required_properties;

    public GetResourcesListElement() {
    }

    public GetResourcesListElement(
           _0._1.ResourceManagement.toatech.AuthNodeElement user,
           java.lang.String root_resource_id,
           java.lang.String include_children,
           java.lang.Integer select_from,
           java.lang.Integer select_count,
           java.lang.String date,
           java.lang.String[] required_properties) {
           this.user = user;
           this.root_resource_id = root_resource_id;
           this.include_children = include_children;
           this.select_from = select_from;
           this.select_count = select_count;
           this.date = date;
           this.required_properties = required_properties;
    }


    /**
     * Gets the user value for this GetResourcesListElement.
     * 
     * @return user
     */
    public _0._1.ResourceManagement.toatech.AuthNodeElement getUser() {
        return user;
    }


    /**
     * Sets the user value for this GetResourcesListElement.
     * 
     * @param user
     */
    public void setUser(_0._1.ResourceManagement.toatech.AuthNodeElement user) {
        this.user = user;
    }


    /**
     * Gets the root_resource_id value for this GetResourcesListElement.
     * 
     * @return root_resource_id
     */
    public java.lang.String getRoot_resource_id() {
        return root_resource_id;
    }


    /**
     * Sets the root_resource_id value for this GetResourcesListElement.
     * 
     * @param root_resource_id
     */
    public void setRoot_resource_id(java.lang.String root_resource_id) {
        this.root_resource_id = root_resource_id;
    }


    /**
     * Gets the include_children value for this GetResourcesListElement.
     * 
     * @return include_children
     */
    public java.lang.String getInclude_children() {
        return include_children;
    }


    /**
     * Sets the include_children value for this GetResourcesListElement.
     * 
     * @param include_children
     */
    public void setInclude_children(java.lang.String include_children) {
        this.include_children = include_children;
    }


    /**
     * Gets the select_from value for this GetResourcesListElement.
     * 
     * @return select_from
     */
    public java.lang.Integer getSelect_from() {
        return select_from;
    }


    /**
     * Sets the select_from value for this GetResourcesListElement.
     * 
     * @param select_from
     */
    public void setSelect_from(java.lang.Integer select_from) {
        this.select_from = select_from;
    }


    /**
     * Gets the select_count value for this GetResourcesListElement.
     * 
     * @return select_count
     */
    public java.lang.Integer getSelect_count() {
        return select_count;
    }


    /**
     * Sets the select_count value for this GetResourcesListElement.
     * 
     * @param select_count
     */
    public void setSelect_count(java.lang.Integer select_count) {
        this.select_count = select_count;
    }


    /**
     * Gets the date value for this GetResourcesListElement.
     * 
     * @return date
     */
    public java.lang.String getDate() {
        return date;
    }


    /**
     * Sets the date value for this GetResourcesListElement.
     * 
     * @param date
     */
    public void setDate(java.lang.String date) {
        this.date = date;
    }


    /**
     * Gets the required_properties value for this GetResourcesListElement.
     * 
     * @return required_properties
     */
    public java.lang.String[] getRequired_properties() {
        return required_properties;
    }


    /**
     * Sets the required_properties value for this GetResourcesListElement.
     * 
     * @param required_properties
     */
    public void setRequired_properties(java.lang.String[] required_properties) {
        this.required_properties = required_properties;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetResourcesListElement)) return false;
        GetResourcesListElement other = (GetResourcesListElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser()))) &&
            ((this.root_resource_id==null && other.getRoot_resource_id()==null) || 
             (this.root_resource_id!=null &&
              this.root_resource_id.equals(other.getRoot_resource_id()))) &&
            ((this.include_children==null && other.getInclude_children()==null) || 
             (this.include_children!=null &&
              this.include_children.equals(other.getInclude_children()))) &&
            ((this.select_from==null && other.getSelect_from()==null) || 
             (this.select_from!=null &&
              this.select_from.equals(other.getSelect_from()))) &&
            ((this.select_count==null && other.getSelect_count()==null) || 
             (this.select_count!=null &&
              this.select_count.equals(other.getSelect_count()))) &&
            ((this.date==null && other.getDate()==null) || 
             (this.date!=null &&
              this.date.equals(other.getDate()))) &&
            ((this.required_properties==null && other.getRequired_properties()==null) || 
             (this.required_properties!=null &&
              java.util.Arrays.equals(this.required_properties, other.getRequired_properties())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        if (getRoot_resource_id() != null) {
            _hashCode += getRoot_resource_id().hashCode();
        }
        if (getInclude_children() != null) {
            _hashCode += getInclude_children().hashCode();
        }
        if (getSelect_from() != null) {
            _hashCode += getSelect_from().hashCode();
        }
        if (getSelect_count() != null) {
            _hashCode += getSelect_count().hashCode();
        }
        if (getDate() != null) {
            _hashCode += getDate().hashCode();
        }
        if (getRequired_properties() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRequired_properties());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRequired_properties(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetResourcesListElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesListElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "AuthNodeElement"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("root_resource_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "root_resource_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("include_children");
        elemField.setXmlName(new javax.xml.namespace.QName("", "include_children"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("select_from");
        elemField.setXmlName(new javax.xml.namespace.QName("", "select_from"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("select_count");
        elemField.setXmlName(new javax.xml.namespace.QName("", "select_count"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date");
        elemField.setXmlName(new javax.xml.namespace.QName("", "date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("required_properties");
        elemField.setXmlName(new javax.xml.namespace.QName("", "required_properties"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "label"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
