/**
 * GetResourcesSkillsElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class GetResourcesSkillsElement  implements java.io.Serializable {
    private _0._1.ResourceManagement.toatech.AuthNodeElement user;

    private java.lang.String expand_complex_skills;

    private _0._1.ResourceManagement.toatech.GetResourcesParamsElement[] resources;

    public GetResourcesSkillsElement() {
    }

    public GetResourcesSkillsElement(
           _0._1.ResourceManagement.toatech.AuthNodeElement user,
           java.lang.String expand_complex_skills,
           _0._1.ResourceManagement.toatech.GetResourcesParamsElement[] resources) {
           this.user = user;
           this.expand_complex_skills = expand_complex_skills;
           this.resources = resources;
    }


    /**
     * Gets the user value for this GetResourcesSkillsElement.
     * 
     * @return user
     */
    public _0._1.ResourceManagement.toatech.AuthNodeElement getUser() {
        return user;
    }


    /**
     * Sets the user value for this GetResourcesSkillsElement.
     * 
     * @param user
     */
    public void setUser(_0._1.ResourceManagement.toatech.AuthNodeElement user) {
        this.user = user;
    }


    /**
     * Gets the expand_complex_skills value for this GetResourcesSkillsElement.
     * 
     * @return expand_complex_skills
     */
    public java.lang.String getExpand_complex_skills() {
        return expand_complex_skills;
    }


    /**
     * Sets the expand_complex_skills value for this GetResourcesSkillsElement.
     * 
     * @param expand_complex_skills
     */
    public void setExpand_complex_skills(java.lang.String expand_complex_skills) {
        this.expand_complex_skills = expand_complex_skills;
    }


    /**
     * Gets the resources value for this GetResourcesSkillsElement.
     * 
     * @return resources
     */
    public _0._1.ResourceManagement.toatech.GetResourcesParamsElement[] getResources() {
        return resources;
    }


    /**
     * Sets the resources value for this GetResourcesSkillsElement.
     * 
     * @param resources
     */
    public void setResources(_0._1.ResourceManagement.toatech.GetResourcesParamsElement[] resources) {
        this.resources = resources;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetResourcesSkillsElement)) return false;
        GetResourcesSkillsElement other = (GetResourcesSkillsElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser()))) &&
            ((this.expand_complex_skills==null && other.getExpand_complex_skills()==null) || 
             (this.expand_complex_skills!=null &&
              this.expand_complex_skills.equals(other.getExpand_complex_skills()))) &&
            ((this.resources==null && other.getResources()==null) || 
             (this.resources!=null &&
              java.util.Arrays.equals(this.resources, other.getResources())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        if (getExpand_complex_skills() != null) {
            _hashCode += getExpand_complex_skills().hashCode();
        }
        if (getResources() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResources());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResources(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetResourcesSkillsElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesSkillsElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "AuthNodeElement"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expand_complex_skills");
        elemField.setXmlName(new javax.xml.namespace.QName("", "expand_complex_skills"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resources");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resources"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesParamsElement"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "resource"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
