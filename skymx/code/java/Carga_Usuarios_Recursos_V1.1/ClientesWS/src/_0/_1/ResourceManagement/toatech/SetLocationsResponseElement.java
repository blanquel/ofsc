/**
 * SetLocationsResponseElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class SetLocationsResponseElement  implements java.io.Serializable {
    private int result_code;

    private java.lang.String error_msg;

    private _0._1.ResourceManagement.toatech.LocationResultElement[] location_results;

    public SetLocationsResponseElement() {
    }

    public SetLocationsResponseElement(
           int result_code,
           java.lang.String error_msg,
           _0._1.ResourceManagement.toatech.LocationResultElement[] location_results) {
           this.result_code = result_code;
           this.error_msg = error_msg;
           this.location_results = location_results;
    }


    /**
     * Gets the result_code value for this SetLocationsResponseElement.
     * 
     * @return result_code
     */
    public int getResult_code() {
        return result_code;
    }


    /**
     * Sets the result_code value for this SetLocationsResponseElement.
     * 
     * @param result_code
     */
    public void setResult_code(int result_code) {
        this.result_code = result_code;
    }


    /**
     * Gets the error_msg value for this SetLocationsResponseElement.
     * 
     * @return error_msg
     */
    public java.lang.String getError_msg() {
        return error_msg;
    }


    /**
     * Sets the error_msg value for this SetLocationsResponseElement.
     * 
     * @param error_msg
     */
    public void setError_msg(java.lang.String error_msg) {
        this.error_msg = error_msg;
    }


    /**
     * Gets the location_results value for this SetLocationsResponseElement.
     * 
     * @return location_results
     */
    public _0._1.ResourceManagement.toatech.LocationResultElement[] getLocation_results() {
        return location_results;
    }


    /**
     * Sets the location_results value for this SetLocationsResponseElement.
     * 
     * @param location_results
     */
    public void setLocation_results(_0._1.ResourceManagement.toatech.LocationResultElement[] location_results) {
        this.location_results = location_results;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetLocationsResponseElement)) return false;
        SetLocationsResponseElement other = (SetLocationsResponseElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.result_code == other.getResult_code() &&
            ((this.error_msg==null && other.getError_msg()==null) || 
             (this.error_msg!=null &&
              this.error_msg.equals(other.getError_msg()))) &&
            ((this.location_results==null && other.getLocation_results()==null) || 
             (this.location_results!=null &&
              java.util.Arrays.equals(this.location_results, other.getLocation_results())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getResult_code();
        if (getError_msg() != null) {
            _hashCode += getError_msg().hashCode();
        }
        if (getLocation_results() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLocation_results());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLocation_results(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetLocationsResponseElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetLocationsResponseElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("result_code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "result_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("error_msg");
        elemField.setXmlName(new javax.xml.namespace.QName("", "error_msg"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location_results");
        elemField.setXmlName(new javax.xml.namespace.QName("", "location_results"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationResultElement"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "location_result"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
