/**
 * SetResourcesLocationsElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class SetResourcesLocationsElement  implements java.io.Serializable {
    private _0._1.ResourceManagement.toatech.AuthNodeElement user;

    private _0._1.ResourceManagement.toatech.ResourceLocationElement[] resource_locations;

    public SetResourcesLocationsElement() {
    }

    public SetResourcesLocationsElement(
           _0._1.ResourceManagement.toatech.AuthNodeElement user,
           _0._1.ResourceManagement.toatech.ResourceLocationElement[] resource_locations) {
           this.user = user;
           this.resource_locations = resource_locations;
    }


    /**
     * Gets the user value for this SetResourcesLocationsElement.
     * 
     * @return user
     */
    public _0._1.ResourceManagement.toatech.AuthNodeElement getUser() {
        return user;
    }


    /**
     * Sets the user value for this SetResourcesLocationsElement.
     * 
     * @param user
     */
    public void setUser(_0._1.ResourceManagement.toatech.AuthNodeElement user) {
        this.user = user;
    }


    /**
     * Gets the resource_locations value for this SetResourcesLocationsElement.
     * 
     * @return resource_locations
     */
    public _0._1.ResourceManagement.toatech.ResourceLocationElement[] getResource_locations() {
        return resource_locations;
    }


    /**
     * Sets the resource_locations value for this SetResourcesLocationsElement.
     * 
     * @param resource_locations
     */
    public void setResource_locations(_0._1.ResourceManagement.toatech.ResourceLocationElement[] resource_locations) {
        this.resource_locations = resource_locations;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetResourcesLocationsElement)) return false;
        SetResourcesLocationsElement other = (SetResourcesLocationsElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              this.user.equals(other.getUser()))) &&
            ((this.resource_locations==null && other.getResource_locations()==null) || 
             (this.resource_locations!=null &&
              java.util.Arrays.equals(this.resource_locations, other.getResource_locations())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUser() != null) {
            _hashCode += getUser().hashCode();
        }
        if (getResource_locations() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResource_locations());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResource_locations(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetResourcesLocationsElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesLocationsElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "AuthNodeElement"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resource_locations");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resource_locations"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceLocationElement"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "resource_location"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
