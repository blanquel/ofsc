/**
 * ResourceLocationElement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class ResourceLocationElement  implements java.io.Serializable {
    private java.lang.String resource_id;

    private java.lang.String location_label;

    private java.lang.String type;

    private java.lang.String[] weekdays;

    public ResourceLocationElement() {
    }

    public ResourceLocationElement(
           java.lang.String resource_id,
           java.lang.String location_label,
           java.lang.String type,
           java.lang.String[] weekdays) {
           this.resource_id = resource_id;
           this.location_label = location_label;
           this.type = type;
           this.weekdays = weekdays;
    }


    /**
     * Gets the resource_id value for this ResourceLocationElement.
     * 
     * @return resource_id
     */
    public java.lang.String getResource_id() {
        return resource_id;
    }


    /**
     * Sets the resource_id value for this ResourceLocationElement.
     * 
     * @param resource_id
     */
    public void setResource_id(java.lang.String resource_id) {
        this.resource_id = resource_id;
    }


    /**
     * Gets the location_label value for this ResourceLocationElement.
     * 
     * @return location_label
     */
    public java.lang.String getLocation_label() {
        return location_label;
    }


    /**
     * Sets the location_label value for this ResourceLocationElement.
     * 
     * @param location_label
     */
    public void setLocation_label(java.lang.String location_label) {
        this.location_label = location_label;
    }


    /**
     * Gets the type value for this ResourceLocationElement.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this ResourceLocationElement.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the weekdays value for this ResourceLocationElement.
     * 
     * @return weekdays
     */
    public java.lang.String[] getWeekdays() {
        return weekdays;
    }


    /**
     * Sets the weekdays value for this ResourceLocationElement.
     * 
     * @param weekdays
     */
    public void setWeekdays(java.lang.String[] weekdays) {
        this.weekdays = weekdays;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResourceLocationElement)) return false;
        ResourceLocationElement other = (ResourceLocationElement) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.resource_id==null && other.getResource_id()==null) || 
             (this.resource_id!=null &&
              this.resource_id.equals(other.getResource_id()))) &&
            ((this.location_label==null && other.getLocation_label()==null) || 
             (this.location_label!=null &&
              this.location_label.equals(other.getLocation_label()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.weekdays==null && other.getWeekdays()==null) || 
             (this.weekdays!=null &&
              java.util.Arrays.equals(this.weekdays, other.getWeekdays())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getResource_id() != null) {
            _hashCode += getResource_id().hashCode();
        }
        if (getLocation_label() != null) {
            _hashCode += getLocation_label().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getWeekdays() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getWeekdays());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getWeekdays(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResourceLocationElement.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceLocationElement"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resource_id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resource_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("location_label");
        elemField.setXmlName(new javax.xml.namespace.QName("", "location_label"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("weekdays");
        elemField.setXmlName(new javax.xml.namespace.QName("", "weekdays"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "wd"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
