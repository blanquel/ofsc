/**
 * ResourceManagementPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public interface ResourceManagementPort extends java.rmi.Remote {
    public _0._1.ResourceManagement.toatech.SetUserResponseElement insert_user(_0._1.ResourceManagement.toatech.InsertUserElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetUserResponseElement update_user(_0._1.ResourceManagement.toatech.UpdateUserElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetUserResponseElement delete_user(_0._1.ResourceManagement.toatech.DeleteUserElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetUserResponseElement get_user(_0._1.ResourceManagement.toatech.GetUserElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetUsersListResponseElement get_users_list(_0._1.ResourceManagement.toatech.GetUsersListElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetResourceResponseElement insert_resource(_0._1.ResourceManagement.toatech.InsertResourceElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetResourceResponseElement update_resource(_0._1.ResourceManagement.toatech.UpdateResourceElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetResourceResponseElement get_resource(_0._1.ResourceManagement.toatech.GetResourceElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetResourcesListResponseElement get_resources_list(_0._1.ResourceManagement.toatech.GetResourcesListElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement set_resources_calendars(_0._1.ResourceManagement.toatech.SetResourcesCalendarsElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetResourcesCalendarsResponseElement get_resources_calendars(_0._1.ResourceManagement.toatech.GetResourcesCalendarsElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetResourcesSkillsResponseElement set_resources_skills(_0._1.ResourceManagement.toatech.SetResourcesSkillsElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetResourcesSkillsResponseElement get_resources_skills(_0._1.ResourceManagement.toatech.GetResourcesSkillsElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement set_resources_zones(_0._1.ResourceManagement.toatech.SetResourcesZonesElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetResourcesZonesResponseElement get_resources_zones(_0._1.ResourceManagement.toatech.GetResourcesZonesElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetLocationsResponseElement update_locations(_0._1.ResourceManagement.toatech.SetLocationsElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetLocationsResponseElement delete_locations(_0._1.ResourceManagement.toatech.SetLocationsElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetLocationsResponseElement get_locations(_0._1.ResourceManagement.toatech.GetLocationsElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.SetResourcesLocationsResponseElement set_resources_locations(_0._1.ResourceManagement.toatech.SetResourcesLocationsElement params) throws java.rmi.RemoteException;
    public _0._1.ResourceManagement.toatech.GetResourcesLocationsResponseElement get_resources_locations(_0._1.ResourceManagement.toatech.GetResourcesLocationsElement params) throws java.rmi.RemoteException;
}
