/**
 * ResourceManagementBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package _0._1.ResourceManagement.toatech;

public class ResourceManagementBindingStub extends org.apache.axis.client.Stub implements _0._1.ResourceManagement.toatech.ResourceManagementPort {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[20];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("insert_user");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "insert_user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "InsertUserElement"), _0._1.ResourceManagement.toatech.InsertUserElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetUserResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetUserResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "insert_user_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("update_user");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "update_user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "UpdateUserElement"), _0._1.ResourceManagement.toatech.UpdateUserElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetUserResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetUserResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "update_user_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("delete_user");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "delete_user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "DeleteUserElement"), _0._1.ResourceManagement.toatech.DeleteUserElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetUserResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetUserResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "delete_user_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_user");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_user"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUserElement"), _0._1.ResourceManagement.toatech.GetUserElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUserResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetUserResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_user_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_users_list");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_users_list"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUsersListElement"), _0._1.ResourceManagement.toatech.GetUsersListElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUsersListResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetUsersListResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_users_list_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("insert_resource");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "insert_resource"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "InsertResourceElement"), _0._1.ResourceManagement.toatech.InsertResourceElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourceResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetResourceResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "insert_resource_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("update_resource");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "update_resource"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "UpdateResourceElement"), _0._1.ResourceManagement.toatech.UpdateResourceElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourceResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetResourceResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "update_resource_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_resource");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resource"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourceElement"), _0._1.ResourceManagement.toatech.GetResourceElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourceResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetResourceResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resource_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_resources_list");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_list"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesListElement"), _0._1.ResourceManagement.toatech.GetResourcesListElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesListResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetResourcesListResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_list_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("set_resources_calendars");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "set_resources_calendars"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesCalendarsElement"), _0._1.ResourceManagement.toatech.SetResourcesCalendarsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesCalendarsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "set_resources_calendars_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_resources_calendars");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_calendars"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesCalendarsElement"), _0._1.ResourceManagement.toatech.GetResourcesCalendarsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesCalendarsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetResourcesCalendarsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_calendars_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("set_resources_skills");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "set_resources_skills"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesSkillsElement"), _0._1.ResourceManagement.toatech.SetResourcesSkillsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesSkillsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetResourcesSkillsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "set_resources_skills_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_resources_skills");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_skills"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesSkillsElement"), _0._1.ResourceManagement.toatech.GetResourcesSkillsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesSkillsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetResourcesSkillsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_skills_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("set_resources_zones");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "set_resources_zones"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesZonesElement"), _0._1.ResourceManagement.toatech.SetResourcesZonesElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesZonesResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "set_resources_zones_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_resources_zones");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_zones"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesZonesElement"), _0._1.ResourceManagement.toatech.GetResourcesZonesElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesZonesResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetResourcesZonesResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_zones_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("update_locations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "update_locations"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetLocationsElement"), _0._1.ResourceManagement.toatech.SetLocationsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetLocationsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetLocationsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "update_locations_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("delete_locations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "delete_locations"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetLocationsElement"), _0._1.ResourceManagement.toatech.SetLocationsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetLocationsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetLocationsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "delete_locations_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_locations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_locations"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetLocationsElement"), _0._1.ResourceManagement.toatech.GetLocationsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetLocationsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetLocationsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_locations_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("set_resources_locations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "set_resources_locations"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesLocationsElement"), _0._1.ResourceManagement.toatech.SetResourcesLocationsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesLocationsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.SetResourcesLocationsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "set_resources_locations_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("get_resources_locations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_locations"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesLocationsElement"), _0._1.ResourceManagement.toatech.GetResourcesLocationsElement.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesLocationsResponseElement"));
        oper.setReturnClass(_0._1.ResourceManagement.toatech.GetResourcesLocationsResponseElement.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "get_resources_locations_response"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    public ResourceManagementBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public ResourceManagementBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public ResourceManagementBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "AuthNodeElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.AuthNodeElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "CalendarElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.CalendarElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "CalendarList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.CalendarElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "CalendarElement");
            qName2 = new javax.xml.namespace.QName("", "calendar");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "CalendarResultElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.CalendarResultElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "CalendarResultList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.CalendarResultElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "CalendarResultElement");
            qName2 = new javax.xml.namespace.QName("", "calendar_result");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "DeleteUserElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.DeleteUserElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetLocationsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetLocationsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetLocationsResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetLocationsResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourceElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourceElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourceResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourceResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesCalendarsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesCalendarsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesCalendarsResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesCalendarsResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesListElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesListElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesListResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesListResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesLocationsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesLocationsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesLocationsResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesLocationsResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesParamsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesParamsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesParamsList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesParamsElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesParamsElement");
            qName2 = new javax.xml.namespace.QName("", "resource");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesSkillsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesSkillsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesSkillsResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesSkillsResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesZonesElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesZonesElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetResourcesZonesResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetResourcesZonesResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUserElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetUserElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUserResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetUserResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUsersListElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetUsersListElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "GetUsersListResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.GetUsersListResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "InsertResourceElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.InsertResourceElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "InsertUserElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.InsertUserElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.LocationElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.LocationElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationElement");
            qName2 = new javax.xml.namespace.QName("", "location");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationResourceParams");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.LocationResourceParams.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationResourceParamsList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.LocationResourceParams[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationResourceParams");
            qName2 = new javax.xml.namespace.QName("", "resource");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationResultElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.LocationResultElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "PropertiesElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.PropertyElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "PropertyElement");
            qName2 = new javax.xml.namespace.QName("", "property");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "PropertyElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.PropertyElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "RequiredPropertiesElement");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("", "label");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceIdListElement");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("", "id");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceListElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.ResourceListItemElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceListItemElement");
            qName2 = new javax.xml.namespace.QName("", "resource");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceListItemElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.ResourceListItemElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceLocationElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.ResourceLocationElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceLocationList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.ResourceLocationElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ResourceLocationElement");
            qName2 = new javax.xml.namespace.QName("", "resource_location");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetLocationResultList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.LocationResultElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationResultElement");
            qName2 = new javax.xml.namespace.QName("", "location_result");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetLocationsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetLocationsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetLocationsResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetLocationsResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourceResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourceResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesCalendarsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourcesCalendarsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesCalendarsResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesLocationsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourcesLocationsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesLocationsResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourcesLocationsResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesLocationsResultList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.LocationResultElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "LocationResultElement");
            qName2 = new javax.xml.namespace.QName("", "resource_location_result");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesSkillsElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourcesSkillsElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesSkillsResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourcesSkillsResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesZonesElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourcesZonesElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetResourcesZonesResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SetUserResponseElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SetUserResponseElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SkillElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SkillElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SkillList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SkillElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SkillElement");
            qName2 = new javax.xml.namespace.QName("", "skill");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SkillResultElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SkillResultElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SkillResultList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.SkillResultElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "SkillResultElement");
            qName2 = new javax.xml.namespace.QName("", "skill_result");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "TextPropertiesElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.TextPropertyElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "TextPropertyElement");
            qName2 = new javax.xml.namespace.QName("", "property");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "TextPropertyElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.TextPropertyElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "UpdateResourceElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.UpdateResourceElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "UpdateUserElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.UpdateUserElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "UserListElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.UserListItemElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "UserListItemElement");
            qName2 = new javax.xml.namespace.QName("", "user");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "UserListItemElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.UserListItemElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "UsersLoginList");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("", "login");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "WeekdaysList");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("", "wd");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "WorkSkillElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.WorkSkillElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "WorkSkillList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.WorkSkillList.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ZoneElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.ZoneElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ZoneList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.ZoneElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ZoneElement");
            qName2 = new javax.xml.namespace.QName("", "zone");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ZoneResultElement");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.ZoneResultElement.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ZoneResultList");
            cachedSerQNames.add(qName);
            cls = _0._1.ResourceManagement.toatech.ZoneResultElement[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:toatech:ResourceManagement:1.0", "ZoneResultElement");
            qName2 = new javax.xml.namespace.QName("", "zone_result");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public _0._1.ResourceManagement.toatech.SetUserResponseElement insert_user(_0._1.ResourceManagement.toatech.InsertUserElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/insert_user");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "insert_user"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetUserResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetUserResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetUserResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetUserResponseElement update_user(_0._1.ResourceManagement.toatech.UpdateUserElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/update_user");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "update_user"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetUserResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetUserResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetUserResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetUserResponseElement delete_user(_0._1.ResourceManagement.toatech.DeleteUserElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/delete_user");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "delete_user"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetUserResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetUserResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetUserResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetUserResponseElement get_user(_0._1.ResourceManagement.toatech.GetUserElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_user");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_user"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetUserResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetUserResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetUserResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetUsersListResponseElement get_users_list(_0._1.ResourceManagement.toatech.GetUsersListElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_users_list");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_users_list"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetUsersListResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetUsersListResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetUsersListResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetResourceResponseElement insert_resource(_0._1.ResourceManagement.toatech.InsertResourceElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/insert_resource");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "insert_resource"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetResourceResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetResourceResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetResourceResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetResourceResponseElement update_resource(_0._1.ResourceManagement.toatech.UpdateResourceElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/update_resource");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "update_resource"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetResourceResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetResourceResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetResourceResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetResourceResponseElement get_resource(_0._1.ResourceManagement.toatech.GetResourceElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_resource");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_resource"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetResourceResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetResourceResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetResourceResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetResourcesListResponseElement get_resources_list(_0._1.ResourceManagement.toatech.GetResourcesListElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_resources_list");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_resources_list"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetResourcesListResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetResourcesListResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetResourcesListResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement set_resources_calendars(_0._1.ResourceManagement.toatech.SetResourcesCalendarsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/set_resources_calendars");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "set_resources_calendars"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetResourcesCalendarsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetResourcesCalendarsResponseElement get_resources_calendars(_0._1.ResourceManagement.toatech.GetResourcesCalendarsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_resources_calendars");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_resources_calendars"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetResourcesCalendarsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetResourcesCalendarsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetResourcesCalendarsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetResourcesSkillsResponseElement set_resources_skills(_0._1.ResourceManagement.toatech.SetResourcesSkillsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/set_resources_skills");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "set_resources_skills"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetResourcesSkillsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetResourcesSkillsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetResourcesSkillsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetResourcesSkillsResponseElement get_resources_skills(_0._1.ResourceManagement.toatech.GetResourcesSkillsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_resources_skills");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_resources_skills"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetResourcesSkillsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetResourcesSkillsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetResourcesSkillsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement set_resources_zones(_0._1.ResourceManagement.toatech.SetResourcesZonesElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/set_resources_zones");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "set_resources_zones"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetResourcesZonesResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetResourcesZonesResponseElement get_resources_zones(_0._1.ResourceManagement.toatech.GetResourcesZonesElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_resources_zones");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_resources_zones"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetResourcesZonesResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetResourcesZonesResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetResourcesZonesResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetLocationsResponseElement update_locations(_0._1.ResourceManagement.toatech.SetLocationsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/update_locations");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "update_locations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetLocationsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetLocationsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetLocationsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetLocationsResponseElement delete_locations(_0._1.ResourceManagement.toatech.SetLocationsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/delete_locations");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "delete_locations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetLocationsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetLocationsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetLocationsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetLocationsResponseElement get_locations(_0._1.ResourceManagement.toatech.GetLocationsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_locations");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_locations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetLocationsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetLocationsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetLocationsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.SetResourcesLocationsResponseElement set_resources_locations(_0._1.ResourceManagement.toatech.SetResourcesLocationsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/set_resources_locations");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "set_resources_locations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.SetResourcesLocationsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.SetResourcesLocationsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.SetResourcesLocationsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public _0._1.ResourceManagement.toatech.GetResourcesLocationsResponseElement get_resources_locations(_0._1.ResourceManagement.toatech.GetResourcesLocationsElement params) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("ResourceManagementService/get_resources_locations");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "get_resources_locations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {params});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (_0._1.ResourceManagement.toatech.GetResourcesLocationsResponseElement) _resp;
            } catch (java.lang.Exception _exception) {
                return (_0._1.ResourceManagement.toatech.GetResourcesLocationsResponseElement) org.apache.axis.utils.JavaUtils.convert(_resp, _0._1.ResourceManagement.toatech.GetResourcesLocationsResponseElement.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
