var data = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:toa:capacity\">\r\n   <soapenv:Header/>\r\n   <soapenv:Body>\r\n      <urn:get_capacity>\r\n               <user>\r\n\t<now>2018-05-03T21:16:51.651Z</now>\r\n<login>SOAP</login>\r\n<company>sky-mx2.test</company>\r\n<auth_string>5ae8da31c8c01dbd2df13bc3d09caef69af682fed4e2b5305075697bd259a49e</auth_string>\t \r\n\t\t</user> \r\n         \r\n         <location>IE00170710</location>\r\n         <date>2018-02-10</date>\r\n         <determine_location_by_work_zone>true</determine_location_by_work_zone>\r\n         <dont_aggregate_results>true</dont_aggregate_results>\r\n        \r\n         <activity_field>\r\n            <name>czip</name>\r\n            <value>03010</value>\r\n         </activity_field>\r\n      </urn:get_capacity>\r\n   </soapenv:Body>\r\n</soapenv:Envelope>";

var xhr = new XMLHttpRequest();
xhr.withCredentials = true;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
    console.log(this.responseText);
  }
});

xhr.open("POST", "https://api.etadirect.com/soap/inbound/");
xhr.setRequestHeader("Content-Type", "text/xml");
xhr.setRequestHeader("Cache-Control", "no-cache");
// xhr.setRequestHeader("Postman-Token", "02376e36-39a1-421f-ad51-5846e8dc4ab1");

xhr.send(data);