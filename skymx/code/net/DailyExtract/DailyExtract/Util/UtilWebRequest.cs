﻿using RestSharp;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
//using DailyExtract.Models;

namespace DailyExtract.Util
{
    public enum enumMethod
    {
        GET,
        POST,
        DELETE,
        PUT,
        PATCH
    }
    public class UtilWebRequest
    {
        // DEV
        const string Authorization = "Basic YThhM2VjMmIzNjQxNTkzYTM5MjVlNjQ5MGJjMjg0ZGFlNjhmMDk5MzZAc2t5LW14Mi50ZXN0OjIyOTc5NTkwZjI0OTEwYTY4NDgwZjQ4MzcyYzFhOGE4NmVlNWM3OTdjMjI5Y2U5MjE1ZmFkZmNlN2FhZjE1YzY=";

        // QA
        //  const string Authorization = "Basic YThhM2VjMmIzNjQxNTkzYTM5MjVlNjQ5MGJjMjg0ZGFlNjhmMDk5MzZAc2t5LW14MS50ZXN0OjI2YzEzMTIzOTlhMDg5MzA2NzMwYzc5ZjFkM2Q3MjNhMGNjZGYxZWQwZDRiZmQwNWFjODBlYTcyMzRjMGVmYWE=";
      

        public static ResponseOFSC SendWayAsync(string endpoint, enumMethod enumMethod, string data)
        {
            var client = new RestClient("https://api.etadirect.com/" + endpoint);
            RestRequest request = new RestRequest();

            switch (enumMethod.ToString())
            {
                case "PUT":
                    request.Method = Method.PUT;
                    break;

                case "POST":
                    request.Method = Method.POST;
                    break;
                case "PATCH":
                    request.Method = Method.PATCH;
                    break;
                case "GET":
                    request.Method = Method.GET;
                    break;
                case "DELETE":
                    request.Method = Method.DELETE;
                    break;
                default:
                    break;
            }

            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Authorization", Authorization);
            request.AddHeader("Content-Type", "application/json");
            if (!string.IsNullOrEmpty(data))
                request.AddParameter("undefined", data, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            ResponseOFSC responseOFSC = new ResponseOFSC();
            responseOFSC.Content = response.Content;
            responseOFSC.statusCode = (int)response.StatusCode;
            responseOFSC.ErrorMessage = response.ErrorMessage;
            return responseOFSC;
        }
    }
}
