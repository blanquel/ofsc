﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargaArbolOFSC.Models
{
    public class WorkSkill
    {
        public string workSkill { get; set; }
        public string ratio { get; set; } = "100";
        public string startDate { get; set; } = DateTime.Now.ToString("yyyy-MM-dd");
    }
}
