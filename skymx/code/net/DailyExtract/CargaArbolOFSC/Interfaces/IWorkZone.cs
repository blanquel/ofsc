﻿using CargaArbolOFSC.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CargaArbolOFSC.Controller
{
    interface IWorkZone
    {
        Task<bool> ExistAsync(string workZone);

        bool Add(string resource, string workZone);

        List<string> Ranges(WorkZone workZone);

        bool Create(WorkZone workZone);

        bool Exist(WorkZone workZone);

        bool Set(WorkZone workZone);

        //List;

    }
}
