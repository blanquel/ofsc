﻿using RestSharp;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using CargaArbolOFSC.Models;
namespace CargaArbolOFSC
{
    public enum enumMethod
    {
        GET,
        POST,
        DELETE,
        PUT,
        PATCH
    }


    public class UtilWebRequest
    {
        // DEV
        const string Authorization = "Basic YThhM2VjMmIzNjQxNTkzYTM5MjVlNjQ5MGJjMjg0ZGFlNjhmMDk5MzZAc2t5LW14Mi50ZXN0OjIyOTc5NTkwZjI0OTEwYTY4NDgwZjQ4MzcyYzFhOGE4NmVlNWM3OTdjMjI5Y2U5MjE1ZmFkZmNlN2FhZjE1YzY=";

        // QA
        //  const string Authorization = "Basic YThhM2VjMmIzNjQxNTkzYTM5MjVlNjQ5MGJjMjg0ZGFlNjhmMDk5MzZAc2t5LW14MS50ZXN0OjI2YzEzMTIzOTlhMDg5MzA2NzMwYzc5ZjFkM2Q3MjNhMGNjZGYxZWQwZDRiZmQwNWFjODBlYTcyMzRjMGVmYWE=";
        public static async Task<string> GetAsync(string url, enumMethod aenumMethod, string objJson)
        {
            string result = string.Empty;
            var webRequest = System.Net.WebRequest.Create(url);
            // string json = "{" + "\"tracking\": {" + "\"slug\":\"" + Courier + "\"," + "\"tracking_number\":\"" + trackNumber + "\"}}";

            try
            {
                if (webRequest != null)
                {
                    webRequest.Method = aenumMethod.ToString();
                    webRequest.Timeout = 18000;
                    webRequest.ContentType = "application/json";
                    webRequest.Headers.Add("Authorization", Authorization);

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            result = await sr.ReadToEndAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }


        public static async Task<string> GetGenericAsync(string url)
        {
            string result = string.Empty;
            var webRequest = System.Net.WebRequest.Create(url);
            // string json = "{" + "\"tracking\": {" + "\"slug\":\"" + Courier + "\"," + "\"tracking_number\":\"" + trackNumber + "\"}}";

            try
            {
                if (webRequest != null)
                {
                    webRequest.Method = enumMethod.GET.ToString();
                    webRequest.Timeout = 18000;
                    webRequest.ContentType = "application/json";

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        //  total = s.Length;


                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            result = await sr.ReadToEndAsync();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return result;
        }


        public static async Task<string> SendAsync()
        {
            string result = string.Empty;

            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://url");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(await httpWebRequest.GetRequestStreamAsync()))
                {
                    string json = "{\"user\":\"test\"," +
                                  "\"password\":\"bla\"}";

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static ResponseOFSC SendWayAsync(string endpoint, enumMethod enumMethod, string data)
        {
            var client = new RestClient("https://api.etadirect.com/" + endpoint);
            RestRequest request = new RestRequest();

            switch (enumMethod.ToString())
            {
                case "PUT":
                    request.Method = Method.PUT;
                    break;

                case "POST":
                    request.Method = Method.POST;
                    break;
                case "PATCH":
                    request.Method = Method.PATCH;
                    break;
                case "GET":
                    request.Method = Method.GET;
                    break;
                case "DELETE":
                    request.Method = Method.DELETE;
                    break;
                default:
                    break;
            }

            request.AddHeader("Cache-Control", "no-cache");
            request.AddHeader("Authorization", Authorization);
            request.AddHeader("Content-Type", "application/json");
            if (!string.IsNullOrEmpty(data))
                request.AddParameter("undefined", data, ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            ResponseOFSC responseOFSC = new ResponseOFSC();
            responseOFSC.Content = response.Content;
            responseOFSC.statusCode = (int)response.StatusCode;
            return responseOFSC;
        }
    }
}
