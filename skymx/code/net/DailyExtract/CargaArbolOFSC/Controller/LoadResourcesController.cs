﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargaArbolOFSC.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CargaArbolOFSC.Controller
{
    public interface ILoadResourcesController
    {
        bool ResourceExist(Resource resource);
        bool ResourceReplace(Resource resource);
    }

    public interface IWorkSkills
    {
        List<WorkSkill> WorkSkillGet(string externalId);
        bool WorkSkillsCreate(List<WorkSkill> list, string externalId);
    }

    public interface IUser
    {
        User UserGet(string login);
        bool UserCreate(User u);
        bool UserSet(User u);
    }
    public class LoadResourcesController : ILoadResourcesController, IWorkSkills, IUser
    {

        public bool ResourceCreate(Resource resource)
        {
            bool flag = false;
            try
            {

                dynamic objResource = new JObject();
                objResource.resourceId = resource.externalId;
                objResource.organization = resource.organization_id;
                objResource.email = resource.email;
                objResource.phone = resource.phone;
                objResource.status = resource.resourceStatus;
                objResource.parentResourceId = resource.parentId;
                objResource.resourceType = resource.resourceType;
                objResource.name = resource.name;
                objResource.language = resource.language;
                objResource.timeZone = resource.timezone;
                objResource.dateFormat = resource.date_format;
                objResource.timeFormat = resource.time_format;
                objResource.XR_PartnerID = resource.XR_PartnerID;
                objResource.XR_MasterID = resource.XR_MasterID;
                objResource.XR_RPID = resource.XR_RPID;

                // create resource
                var request = UtilWebRequest.SendWayAsync("rest/ofscCore/v1/resources/" + resource.externalId,
                                              enumMethod.PUT,
                                              objResource.ToString());

                // add workskill resources
                List<WorkSkill> listWorkSkills = new List<WorkSkill>();
                listWorkSkills = WorkSkillGet(resource.externalId);

                //foreach (var item in resource.workSkills)
                //listWorkSkills.Add(new WorkSkill { ratio = "100", workSkill = item });

                listWorkSkills.AddRange(resource.workSkills);
                WorkSkillsCreate(listWorkSkills, resource.externalId);


                Console.WriteLine("");

            }
            catch (Exception ex)
            {

                throw;
            }
            return flag;
        }
        public bool ResourceExist(Resource resource)
        {
            try
            {
                var requestResource = UtilWebRequest.SendWayAsync(string.Format("/rest/ofscCore/v1/resources/{0}", resource.externalId), enumMethod.GET, string.Empty);
                if (requestResource.statusCode >= 400)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return false;
            }
        }
        public bool ResourceReplace(Resource resource)
        {
            bool flag = false;
            try
            {

                dynamic objResource = new JObject();
                objResource.resourceId = resource.externalId;
                objResource.organization = resource.organization_id;
                objResource.email = resource.email;
                objResource.phone = resource.phone;
                objResource.status = resource.resourceStatus;
                objResource.parentResourceId = resource.parentId;
                objResource.resourceType = resource.resourceType;
                objResource.name = resource.name;
                objResource.language = resource.language;
                objResource.timeZone = resource.timezone;
                objResource.dateFormat = resource.date_format;
                objResource.timeFormat = resource.time_format;
                objResource.XR_PartnerID = resource.XR_PartnerID;
                objResource.XR_MasterID = resource.XR_MasterID;
                objResource.XR_RPID = resource.XR_RPID;

                // create resource
                var request = UtilWebRequest.SendWayAsync("rest/ofscCore/v1/resources/" + resource.externalId,
                                              enumMethod.PATCH,
                                              objResource.ToString());

                // add workskill resources
                List<WorkSkill> listWorkSkills = new List<WorkSkill>();
                listWorkSkills = WorkSkillGet(resource.externalId);
                listWorkSkills.AddRange(resource.workSkills);
                WorkSkillsCreate(listWorkSkills, resource.externalId);

                // check exist user 
                var responseUser = UserGet(resource.user.login);

                if (responseUser == null)
                    UserCreate(resource.user);
                else
                    UserSet(responseUser);
            }
            catch (Exception ex)
            {
                Program.Logger(string.Format("workzone {0}|{1}|{2}|{3}|{4}|", resource.externalId, resource.email, resource.organization_id, ex.Message, ex.InnerException.Message));
            }
            return flag;
        }
        public bool WorkSkillsCreate(List<WorkSkill> list, string externalId)
        {
            try
            {
                // add workskill resources
                JArray jArray = new JArray();
                foreach (var item in list)
                {
                    dynamic objWorkSkill = new JObject();
                    objWorkSkill.workSkill = item.workSkill;
                    objWorkSkill.ratio = item.ratio;
                    objWorkSkill.startDate = item.startDate;
                    jArray.Add(objWorkSkill);
                }

                if (jArray.Count > 0)
                {
                    var requestWorkSkills = UtilWebRequest.SendWayAsync(string.Format("/rest/ofscCore/v1/resources/{0}/workSkills", externalId),
                                                                        enumMethod.POST,
                                                                        jArray.ToString());
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<WorkSkill> WorkSkillGet(string externalId)
        {
            List<WorkSkill> workSkills = new List<WorkSkill>();
            try
            {
                var requestResource = UtilWebRequest.SendWayAsync(string.Format("/rest/ofscCore/v1/resources/{0}/workSkills", externalId), enumMethod.GET, string.Empty);
                if (requestResource.statusCode >= 400)
                    return null;
                else
                {
                    //var resp = JsonConvert.DeserializeObject<List<string>>(requestResource.Content);
                    JObject resultado = JObject.Parse(requestResource.Content);
                    var resultadoB = resultado["items"].Children().ToList();
                    //var WorkSkills = JsonConvert.DeserializeObject<WorkSkill>(requestResource.Content);

                    foreach (var item in resultadoB)
                    {
                        var WorkSkills = JsonConvert.DeserializeObject<WorkSkill>(item.ToString());
                        workSkills.Add(WorkSkills);
                    }
                    return workSkills;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return null;
            }
        }
        public User UserGet(string login)
        {
            User user = new User();
            try
            {
                var requestResource = UtilWebRequest.SendWayAsync(string.Format("/rest/ofscCore/v1/users/{0}", login),
                                                                  enumMethod.GET,
                                                                  string.Empty);
                if (requestResource.statusCode >= 400)
                    return null;
                else
                {
                    user = JsonConvert.DeserializeObject<User>(requestResource.Content);
                    return user;
                }
            }
            catch (Exception ex)
            {
                Program.Logger(string.Format("user {0}|{1}|{2}|{3}|{4}|", user.login, user.name, user.status, ex.Message, ex.InnerException.Message));
                return null;
            }
        }

        public bool UserCreate(User user)
        {
            bool flag = false;
            try
            {
                dynamic objUser = new JObject();
                objUser.name = user.name;
                objUser.mainResourceId = user.mainResourceId;
                objUser.language = user.language;
                objUser.timeZone = user.timeZone;
                objUser.userType = user.userType;
                objUser.password = user.password;

                // Resources add
                JArray jArray = new JArray(user.mainResourceId);
                objUser.resources = jArray;


                // create resource
                var request = UtilWebRequest.SendWayAsync("rest/ofscCore/v1/users/" + user.login,
                                              enumMethod.PUT,
                                              objUser.ToString());

                flag =  true;


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                flag = false;
            }
            return flag;
        }

        public bool UserSet(User user)
        {
            dynamic objUser = new JObject();
            objUser.name = user.name;
            objUser.mainResourceId = user.mainResourceId;
            objUser.language = user.language;
            objUser.timeZone = user.timeZone;
            objUser.userType = user.userType;
            objUser.password = user.password;

            // Resources add
            JArray jArray = new JArray(user.mainResourceId);
            objUser.resources = jArray;


            // create resource
            var request = UtilWebRequest.SendWayAsync("rest/ofscCore/v1/users/" + user.login,
                                          enumMethod.PATCH,
                                          objUser.ToString());

            return true;
        }
    }
}
