﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CargaArbolOFSC.Models;
using CargaArbolOFSC;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CargaArbolOFSC.Controller
{
    public class WorkZoneController : IWorkZone
    {
        public bool Add(string resource, string workZone)
        {
            bool flag = false;
            try
            {
                DateTime dateTimeStart = DateTime.Now;
                DateTime dateTimeEnd = dateTimeStart.AddYears(1);
                Console.WriteLine(dateTimeStart.ToString("yyyy-MM-dd"));
                dynamic objResource = new JObject();
                objResource.workZone = workZone;
                objResource.startDate = dateTimeStart.ToString("yyyy-MM-dd");
                objResource.endDate = dateTimeEnd.ToString("yyyy-MM-dd");
                objResource.ratio = 100;
                objResource.recurrence = "";


                UtilWebRequest.SendWayAsync("rest/ofscCore/v1/resources/" + resource + "/workZones",
                                            enumMethod.PUT,
                                            objResource.ToString());
                flag = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("WorkZoneController.Add: " + ex.Message);
            }
            return flag;
        }

        public bool Create(WorkZone workZone)
        {
            try
            {
                // create request object
                dynamic objWorkZone = new JObject();
                objWorkZone.workZoneLabel = workZone.workZoneLabel;
                objWorkZone.status = workZone.status;
                objWorkZone.travelArea = workZone.travelArea;
                objWorkZone.workZoneName = workZone.workZoneName;

                JArray jArray = new JArray();
                jArray.Add(workZone.label.FirstOrDefault());
                objWorkZone.keys = jArray;

                var result = UtilWebRequest.SendWayAsync("rest/ofscMetadata/v1/workZones",
                                                         enumMethod.POST,
                                                         objWorkZone.ToString());
                if (result.statusCode == 201)
                    return true;
                if (result.statusCode == 409)
                {
                    var result2 = UtilWebRequest.SendWayAsync("rest/ofscMetadata/v1/workZones/" + workZone.workZoneLabel,
                                                       enumMethod.PUT,
                                                       objWorkZone.ToString());
                    if (result2.statusCode == 200 || result2.statusCode == 201)
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public bool Exist(WorkZone workZone)
        {
            try
            {
                // check exist
                var result = UtilWebRequest.SendWayAsync("rest/ofscMetadata/v1/workZones/" + workZone.label.FirstOrDefault(),
                                            enumMethod.GET,
                                            string.Empty);

                if (result.statusCode == 200)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Concat("Error Excepction {0} details {1} ", ex.Message, ex.InnerException.Message));
                return false;
            }
        }

        public async Task<bool> ExistAsync(string zipCode)
        {
            bool flag = false;
            try
            {
                var result = await UtilWebRequest.GetGenericAsync(string.Format("https://api-codigos-postales.herokuapp.com/v2/codigo_postal/{0}", zipCode));
                dynamic results = JsonConvert.DeserializeObject<dynamic>(result);
                var municipio = results.municipio;

                if (string.IsNullOrEmpty(municipio.Value))
                    flag = false;
                else
                    flag = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return flag;
        }

        public List<string> Ranges(WorkZone workZone)
        {
            workZone.id = new List<string>();
            try
            {
                string[] aWorkZonetmp = workZone.source.Split('|');

                foreach (var item in aWorkZonetmp)
                {
                    if (string.IsNullOrEmpty(item))
                        continue;
                    var restmp = item.Split(',');

                    if (restmp.Count() > 1)
                    {
                        string[] aZipCode = restmp[1].Split('-');
                        if (aZipCode.Count() > 0)
                        {
                            int major = int.Parse(aZipCode[1]);
                            int minor = int.Parse(aZipCode[0]);

                            for (int i = minor; i <= major; i++)
                            {
                                string tmpzipcode = i.ToString().PadLeft(5, '0');
                                workZone.id.Add(tmpzipcode);
                            }
                        }
                    }
                    else
                    {
                        // TODO  hace falta información 
                    }
                }
                List<string> tmpB = new List<string>();
                tmpB.AddRange(workZone.id.Distinct());
                workZone.id.Clear();
                workZone.id.AddRange(tmpB);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message + " " + ex.InnerException);
            }

            return workZone.id;
        }

        public bool Set(WorkZone workZone)
        {
            try
            {
                // create request object
                dynamic objWorkZone = new JObject();
                objWorkZone.workZoneLabel = workZone.workZoneLabel;
                objWorkZone.status = workZone.status;
                objWorkZone.travelArea = workZone.travelArea;
                objWorkZone.workZoneName = workZone.workZoneName;
                JArray jArray = new JArray();
                jArray.Add(workZone.label.FirstOrDefault());
                objWorkZone.keys = jArray;

                var result = UtilWebRequest.SendWayAsync("rest/ofscMetadata/v1/workZones/" + workZone.workZoneLabel,
                                         enumMethod.PUT,
                                         objWorkZone.ToString());
                if (result.statusCode == 200)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Concat("la zona {0} fallo {1} detalles {2}", workZone.workZoneLabel, ex.Message, ex.InnerException));
                return false;

            }
        }
    }
}