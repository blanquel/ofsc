﻿using CargaArbolOFSC.Controller;
using CargaArbolOFSC.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CargaArbolOFSC
{
    public class Program
    {
        public static List<string> list { get; set; } = new List<string>();
        public static List<Resource> listResource { get; set; }
        public static string sPath { get; set; }
        static void Main(string[] args)
        {
            #region Code Load Recursos
            //// Code for read recursos
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();


            //Console.WriteLine(" Ingrese Ubicación (Folder) del archivo");
            //Console.ForegroundColor = ConsoleColor.Yellow;
            //Console.WriteLine(" Por ejemplo C:\\Users\\inmotion\\Documents\\z");
            //Console.ResetColor();
            //sPath = Console.ReadLine();
            //Logger(string.Format("Inicia carga de recursos {0}|{1}||", DateTime.Now, stopwatch.Elapsed.TotalSeconds), sPath);
            ////string sPath = @"C:\Users\inmotion\Documents\z\resourcessky\";
            //if (Directory.Exists(sPath))
            //    Console.WriteLine("Leyendo archivos CSV");
            //else
            //{
            //    Console.Clear();
            //    Console.ForegroundColor = ConsoleColor.Red;
            //    Console.WriteLine(string.Format("Ubicación no valida {0}", sPath));
            //    Thread.Sleep(1800);
            //    Console.ResetColor();
            //    throw new Exception(" x=> { x.id = 'error' }");
            //}
            //// Read csv file
            //ReadCSV(sPath);

            //// Fill object 
            //Split();

            //// Read objects and create resources
            //LoadResourcesController objLoadResourcesController = new LoadResourcesController();

            //foreach (var item in listResource)
            //{
            //    var flag = objLoadResourcesController.ResourceExist(item);

            //    if (flag)
            //        objLoadResourcesController.ResourceReplace(item);
            //    else
            //        objLoadResourcesController.ResourceCreate(item);
            //}

            //stopwatch.Stop();
            //Logger(string.Format("Terminacarga de recursos {0}|{1}||", DateTime.Now, stopwatch.Elapsed.TotalSeconds), sPath);
            //Logger(string.Format("----------------------------------------------------------------------------------"), sPath);

            //Console.WriteLine(string.Format("El proceso tardo en leer {0} archivos en {1} ms", listResource.Count(), stopwatch.ElapsedMilliseconds));
            #endregion

            #region Code Create WorkZones


            Console.WriteLine(" Ingrese Ubicación (Folder) del archivo");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(" Por ejemplo C:\\Users\\inmotion\\Documents\\z");
            Console.ResetColor();
            sPath = Console.ReadLine();
            //  string sPath = @"C:\Users\inmotion\Documents\z\201806\";
            if (Directory.Exists(sPath))
                Console.WriteLine("Leyendo archivos CSV");
            else
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Ubicación no valida {0}", sPath));
                Thread.Sleep(1800);
                Console.ResetColor();
                throw new Exception(" x=> { x.id = 'error' }");
            }

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Logger("----------------------------------------------------------------------------------");
            Logger("Inicio");
            Logger(DateTime.Now.ToString());
            Console.WriteLine("Leyendo archivos CSV");
            // sPath = @"C:\Users\inmotion\Documents\bitbucket\ofsc\skymx\doc\workzones";
            ReadCSV(sPath);

            List<WorkZone> listworkZone = new List<WorkZone>();
            foreach (var item in Program.list)
            {
                string[] result = item.Split(';');
                WorkZone objWorkZone = new WorkZone();
                objWorkZone.workZoneName = result[0];
                objWorkZone.workZoneLabel = result[1];
                objWorkZone.travelArea = result[2];
                objWorkZone.status = result[3];
                objWorkZone.label.Add(result[4]);
                listworkZone.Add(objWorkZone);
            }

            Console.WriteLine("TOTAL DE REGISTROS " + listworkZone.Count());
            Logger(" TOTAL DE REGISTROS " + listworkZone.Count());

            int good = 0;
            int bad = 0;

            var tmpStromg = listworkZone.Skip(24000).ToList();
            var tmp0 = listworkZone.Take(6000).ToList();
            var tmp1 = listworkZone.Skip(6000).Take(6000).ToList();
            var tmp2 = listworkZone.Skip(12000).Take(6000).ToList();
            var tmp3 = listworkZone.Skip(18000).Take(6000).ToList();
            var tmp4 = listworkZone.Skip(24000).Take(6000).ToList();

            Thread threadStrong = new Thread(() => WorkZoneQueue(tmpStromg));
            Thread thread0 = new Thread(() => WorkZoneQueue(tmp0));
            Thread thread1 = new Thread(() => WorkZoneQueue(tmp1));
            Thread thread2 = new Thread(() => WorkZoneQueue(tmp2));
            Thread thread3 = new Thread(() => WorkZoneQueue(tmp3));
            Thread thread4 = new Thread(() => WorkZoneQueue(tmp4));

            thread0.Start();
            thread1.Start();
            thread2.Start();
            thread3.Start();
            thread4.Start();
            threadStrong.Start();
            Console.WriteLine("En proceso " + listworkZone.Count() + " registros .");
            thread0.Join();
            thread1.Join();
            thread2.Join();
            thread3.Join();
            thread4.Join();
            threadStrong.Join();

            Console.WriteLine("TOTAL DE REGISTROS OK " + good);
            Console.WriteLine("TOTAL DE REGISTROS MALOS " + bad);
            stopwatch.Stop();
            Console.WriteLine("Ha terminado");
            Console.WriteLine("Se tardo en Milisegundos " + stopwatch.Elapsed.TotalMilliseconds);
            Console.WriteLine("Se tardo en Segundos " + stopwatch.Elapsed.TotalSeconds);
            Console.WriteLine("Se tardo en Minutos " + stopwatch.Elapsed.TotalMinutes);
            Logger("Milisegundos " + stopwatch.Elapsed.TotalMilliseconds.ToString());
            Logger("Segundos " + stopwatch.Elapsed.TotalSeconds.ToString());
            Logger("Minutos " + stopwatch.Elapsed.TotalMinutes.ToString());
            Logger(DateTime.Now.ToString());
            Logger(" End ");
            Logger("----------------------------------------------------------------------------------");
            Console.ReadLine();
            Console.ReadKey();

            #endregion

        }

        static void ReadCSVTest()
        {
            CSVController objCSVController = new CSVController();
            objCSVController.source = @"C:\Users\inmotion\Documents\z\TestCopy.csv";

            // var test = await objCSVController.LinesFile();
            Task<List<string>> task = objCSVController.LinesFile();
            //   task.Start();
            task.Wait();
            var result = task.Result;
            Program.list = result;
            //objCSVController.LinesFile();
        }

        private static void ReadCSV(string path)
        {
            Console.Clear();
            var files = Directory.GetFiles(path, "*.csv");

            foreach (var item in files)
            {
                try
                {
                    CSVController objCSVController = new CSVController();
                    objCSVController.source = @item;

                    Task<List<string>> task = objCSVController.LinesFile();
                    task.Wait();
                    var result = task.Result;
                    Program.list.AddRange(result);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("Error  al leer el archivo {0} : Exepción :{1}", item, ex.Message));
                }
            }
            Console.Clear();
        }

        static void SplitTest()
        {
            listResource = new List<Resource>();
            foreach (var item in list)
            {
                CSVController objCSVController = new CSVController();
                string[] result = objCSVController.SplitBy(item, ';');
                // Fill object Resource Model
                //Resource res = new Resource();
                var res = FillObject(result);
                listResource.Add(res);
            }
        }

        static void Split()
        {
            listResource = new List<Resource>();
            foreach (var item in list)
            {
                CSVController objCSVController = new CSVController();
                string[] result = objCSVController.SplitBy(item, ';');
                Resource objResource = FillObject(result);
                listResource.Add(objResource);
            }
        }

        static void WorkZoneTest()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            List<Task> tasks = new List<Task>();
            foreach (var item in listResource)
            {
                tasks.Add(Task.Run(() => WorkZoneTestII(item)));
            }
            Task.WaitAll(tasks.ToArray());
            stopwatch.Stop();
            Console.WriteLine("End" + stopwatch.Elapsed.Seconds);

        }

        static void WorkZoneTestII(Resource resource)
        {
            IWorkZone workZoneCtrl = new WorkZoneController();

            Console.WriteLine(resource.externalId + " tiene " + resource.workZone.id.Count() + " items");

            foreach (var itemWorkZone in resource.workZone.id)
            {
                var flag = workZoneCtrl.ExistAsync(itemWorkZone).Result;

                if (flag)
                {
                    workZoneCtrl.Add(resource.externalId, itemWorkZone);
                    Console.WriteLine(" zipcode: " + itemWorkZone + " asigned to item: " + resource.externalId);
                }
            }
        }

        static Resource FillObject(string[] aItems)
        {
            IWorkZone workZone = new WorkZoneController();
            Resource objResource = new Resource();
            objResource.user = new User();
            objResource.workZone = new WorkZone();

            objResource.parentId = aItems[0].Trim();
            objResource.externalId = aItems[1].Trim();
            objResource.name = aItems[2].Trim();
            objResource.resourceType = aItems[3].Trim();
            objResource.resourceStatus = aItems[4];
            objResource.language = aItems[5].Trim();
            objResource.email = aItems[6];
            objResource.phone = aItems[7];
            objResource.timezone = aItems[8];
            objResource.calendar = string.Empty;
            objResource.time_format = aItems[9] == "24 horas" ? "24-hour" : aItems[9];
            objResource.date_format = aItems[10] == "aaaa/mm/dd" ? "yyyy/mm/dd" : aItems[10];
            objResource.resource_workzones = aItems[25];
            objResource.XR_PartnerID = aItems[0];
            objResource.XR_MasterID = (aItems.Count() > 36 ? aItems[36] : string.Empty);

            // start get workskills
            var vwork_skills = aItems[17].Split('|').ToList().Where(x => !string.IsNullOrEmpty(x));
            var work_skills = vwork_skills.ToList();
            foreach (var item in work_skills)
                objResource.workSkills.Add(new WorkSkill() { workSkill = item });
            // end get workskills

            objResource.capacity_categories = aItems[34];
            objResource.XR_RPID = aItems[37];

            if (objResource.resourceType.ToUpper() == "DISP")
                objResource.organization_id = string.IsNullOrEmpty(aItems[11]) ? "SKY" : aItems[11];

            objResource.user.login = aItems[26].Trim();
            objResource.user.userType = aItems[27];
            objResource.user.status = aItems[28];
            objResource.user.language = aItems[29];
            objResource.user.timezone = aItems[31];
            objResource.user.name = aItems[35].ToString();
            objResource.user.sutime_fid = aItems[9];
            objResource.user.sudate_fid = aItems[10];
            objResource.user.sulong_date_fid = "año/mes/día";
            objResource.user.password = "Sky_TOA_2018.";
            objResource.user.passwordTemporary = "yes";
            objResource.user.mainResourceId = aItems[1];
            objResource.user.login_policy = "SEGSKY";
            objResource.workZone.source = objResource.resource_workzones;
            objResource.workZone.id = workZone.Ranges(objResource.workZone);
            return objResource;
        }

        static bool WorkZoneMain(WorkZone workZone)
        {
            bool flag = false;
            IWorkZone ctrlworkZone = new WorkZoneController();
            var checkExist = ctrlworkZone.Exist(workZone);
            if (checkExist)
                flag = ctrlworkZone.Set(workZone);
            else
                flag = ctrlworkZone.Create(workZone);

            return flag;
        }

        static string WorkZoneQueue(List<WorkZone> listworkZone)
        {
            int good = 0;
            int bad = 0;
            int limitTemp = 0;

            foreach (var item in listworkZone)
            {
                limitTemp++;
                var flag = WorkZoneMain(item);
                if (flag)
                    good++;
                else
                {
                    Logger(string.Format("workzone {0}|{1}|{2}|{3}|{4}|", item.workZoneLabel, item.status, item.travelArea, item.workZoneName, item.label.FirstOrDefault()));
                    bad++;
                }

                if (limitTemp == 1000)
                {
                    Thread.Sleep(1000);
                    limitTemp = 0;
                }
            }

            return string.Concat(listworkZone.Count, ",", good, ",", bad);
        }

        //#region Code Create WorkZones


        //Console.WriteLine(" Ingrese Ubicación (Folder) del archivo");
        //Console.ForegroundColor = ConsoleColor.Yellow;
        //Console.WriteLine(" Por ejemplo C:\\Users\\inmotion\\Documents\\z");
        //Console.ResetColor();
        //string sPath = Console.ReadLine();
        ////string sPath = @"C:\Users\inmotion\Documents\z\201806\";
        //if (Directory.Exists(sPath))
        //    Console.WriteLine("Leyendo archivos CSV");
        //else
        //{
        //    Console.Clear();
        //    Console.ForegroundColor = ConsoleColor.Red;
        //    Console.WriteLine(string.Format("Ubicación no valida {0}", sPath));
        //    Thread.Sleep(1800);
        //    Console.ResetColor();
        //    throw new Exception(" x=> { x.id = 'error' }");
        //}

        //Stopwatch stopwatch = new Stopwatch();
        //stopwatch.Start();
        //Logger("----------------------------------------------------------------------------------");
        //Logger("Inicio");
        //Logger(DateTime.Now.ToString());
        //Console.WriteLine("Leyendo archivos CSV");
        //// sPath = @"C:\Users\inmotion\Documents\bitbucket\ofsc\skymx\doc\workzones";
        //ReadCSV(sPath);

        //List<WorkZone> listworkZone = new List<WorkZone>();
        //foreach (var item in Program.list)
        //{
        //    string[] result = item.Split(';');
        //    WorkZone objWorkZone = new WorkZone();
        //    objWorkZone.workZoneName = result[0];
        //    objWorkZone.workZoneLabel = result[1];
        //    objWorkZone.travelArea = result[2];
        //    objWorkZone.status = result[3];
        //    objWorkZone.label.Add(result[4]);
        //    listworkZone.Add(objWorkZone);
        //}

        //Console.WriteLine("TOTAL DE REGISTROS " + listworkZone.Count());
        //Logger(" TOTAL DE REGISTROS " + listworkZone.Count());

        //int good = 0;
        //int bad = 0;

        //var tmpStromg = listworkZone.Skip(24000).ToList();
        //var tmp0 = listworkZone.Take(6000).ToList();
        //var tmp1 = listworkZone.Skip(6000).Take(6000).ToList();
        //var tmp2 = listworkZone.Skip(12000).Take(6000).ToList();
        //var tmp3 = listworkZone.Skip(18000).Take(6000).ToList();
        //var tmp4 = listworkZone.Skip(24000).Take(6000).ToList();

        //Thread threadStrong = new Thread(() => WorkZoneQueue(tmpStromg));
        //Thread thread0 = new Thread(() => WorkZoneQueue(tmp0));
        //Thread thread1 = new Thread(() => WorkZoneQueue(tmp1));
        //Thread thread2 = new Thread(() => WorkZoneQueue(tmp2));
        //Thread thread3 = new Thread(() => WorkZoneQueue(tmp3));
        //Thread thread4 = new Thread(() => WorkZoneQueue(tmp4));

        //thread0.Start();
        //thread1.Start();
        //thread2.Start();
        //thread3.Start();
        //thread4.Start();
        //threadStrong.Start();
        //Console.WriteLine("En proceso " + listworkZone.Count() + " registros .");
        //thread0.Join();
        //thread1.Join();
        //thread2.Join();
        //thread3.Join();
        //thread4.Join();
        //threadStrong.Join();

        //Console.WriteLine("TOTAL DE REGISTROS OK " + good);
        //Console.WriteLine("TOTAL DE REGISTROS MALOS " + bad);
        //stopwatch.Stop();
        //Console.WriteLine("Ha terminado");
        //Console.WriteLine("Se tardo en Milisegundos " + stopwatch.Elapsed.TotalMilliseconds);
        //Console.WriteLine("Se tardo en Segundos " + stopwatch.Elapsed.TotalSeconds);
        //Console.WriteLine("Se tardo en Minutos " + stopwatch.Elapsed.TotalMinutes);
        //Logger("Milisegundos " + stopwatch.Elapsed.TotalMilliseconds.ToString());
        //Logger("Segundos " + stopwatch.Elapsed.TotalSeconds.ToString());
        //Logger("Minutos " + stopwatch.Elapsed.TotalMinutes.ToString());
        //Logger(DateTime.Now.ToString());
        //Logger(" End ");
        //Logger("----------------------------------------------------------------------------------");
        //Console.ReadLine();
        //Console.ReadKey();

        //#endregion




        public static void Logger(String lines)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(@sPath + "\\log.txt", true);
            file.WriteLine(lines);
            file.Close();
        }
    }
}
