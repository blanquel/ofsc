using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WorkZoneLoad.Controller;
using WorkZoneLoad.Models;

namespace WorkZoneLoad
{
    public class Program
    {
        private static List<WorkZone> listWorkZone = new List<WorkZone>();
        private static List<WorkZone> listWorkZoneActive = new List<WorkZone>();
        private static List<WorkZone> listWorkZoneInactive = new List<WorkZone>();

        public static List<string> list { get; set; } = new List<string>();
        public static List<Resource> listResource { get; set; }
        public static string sPath { get; set; } = @"C:\Users\inmotion\Documents\bitbucket\ofsc\skymx\code\html";
        static void Main(string[] args)
        {
            Console.WriteLine(" Ingrese Ubicaci�n (Folder) del archivo");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(" Por ejemplo C:\\Users\\inmotion\\Documents\\z");
            Console.ResetColor();
            // sPath = Console.ReadLine();
            sPath = @"C:\Users\inmotion\Documents\z\resource";
            //  string sPath = @"C:\Users\inmotion\Documents\z\201806\";
            if (Directory.Exists(sPath))
                Console.WriteLine("Leyendo archivos CSV");
            else
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Ubicaci�n no valida {0}", sPath));
                Thread.Sleep(1800);
                Console.ResetColor();
                throw new Exception(" x=> { x.id = 'error' }");
            }
            Console.WriteLine("Obteniendo Zonas de Trabajo disponibles " + DateTime.Now);
            // *****************************************************************
            // Get all workzone
            WorkZoneController ctrlWorkZone = new WorkZoneController();
            listWorkZone = ctrlWorkZone.GetAll();
            Console.WriteLine("Termino de obtener Zonas de Trabajo disponibles " + listWorkZone.Count() + "  " + DateTime.Now);
            listWorkZoneInactive = (listWorkZone.Where(x => x.status == "inactive").ToList());
            listWorkZoneActive = listWorkZone.Where(x => x.status == "active").ToList();
            // end
            // *****************************************************************

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Logger("----------------------------------------------------------------------------------");
            Logger("Inicio");
            Logger(DateTime.Now.ToString());
            Console.WriteLine("Leyendo archivos CSV");
            // sPath = @"C:\Users\inmotion\Documents\bitbucket\ofsc\skymx\doc\workzones";
            ReadCSV(sPath);

            // Fill Object
            Split();

            // recorre recursos y borra sus zonas de trabajo
            int count = 0;
            foreach (var resource in listResource)
            {
                count++;
                // Obtiene zonas de trabajo de recurso
                WorkZoneController workZoneController = new WorkZoneController();
                Console.WriteLine("---------------------------------------------------");
                Console.WriteLine(resource.externalId);


                List<WorkZone> list = workZoneController.Get(resource.externalId);
                Logger("Se van a borrar " + list.Count + " del recurso " + resource.externalId);

                foreach (var item in list)
                    workZoneController.Delete(item, resource.externalId);

                // obtiene rangos 
                List<string> rangos = workZoneController.Ranges(resource.workZone);
                List<WorkZone> listworkZone = new List<WorkZone>();

                Console.WriteLine("TOTAL DE REGISTROS  " + rangos.Count() + " del recurso  " + resource.externalId + ",  recurso " + count + " de  " + listResource.Count);
                Logger("TOTAL DE REGISTROS " + rangos.Count());
                Console.WriteLine("---------------------------------------------------");

                List<string> listTmpWorkZone = new List<string>();
                foreach (var item in rangos)
                {
                    if (listWorkZoneActive.Exists(x => x.workZoneLabel == item))
                    {
                        //Logger("Recurso " + resource.externalId + "      Zona de trabajo " + item);
                        //Console.WriteLine("Recurso " + resource.externalId + "      Zona de trabajo " + item);
                        //workZoneController.Add(resource.externalId, item);
                        listTmpWorkZone.Add(item);
                    }
                    else
                        continue;
                }

                if (listTmpWorkZone.Count > 1000)
                {
                    var tmp0 = listTmpWorkZone.Skip(6000).ToList();
                    var tmp1 = listTmpWorkZone.Skip(3000).Take(3000).ToList();
                    var tmp2 = listTmpWorkZone.Take(3000).ToList();

                    Thread thread0 = new Thread(() => WorkZoneList(resource.externalId, tmp0));
                    Thread thread1 = new Thread(() => WorkZoneList(resource.externalId, tmp1));
                    Thread thread2 = new Thread(() => WorkZoneList(resource.externalId, tmp2));
                    thread0.Start();
                    thread1.Start();
                    thread2.Start();
                    Console.WriteLine("cargando");
                    thread0.Join();
                    thread1.Join();
                    thread2.Join();
                }
                else
                {
                    foreach (string itemWorkZone in listTmpWorkZone)
                        workZoneController.Add(resource.externalId, itemWorkZone);
                }

                // multitask 
                stopwatch.Stop();
                Console.WriteLine("Ha terminado");
                Console.WriteLine("Se tardo en Milisegundos " + stopwatch.Elapsed.TotalMilliseconds);
                Console.WriteLine("Se tardo en Segundos " + stopwatch.Elapsed.TotalSeconds);
                Console.WriteLine("Se tardo en Minutos " + stopwatch.Elapsed.TotalMinutes);
                Console.Clear();
                Logger("Milisegundos " + stopwatch.Elapsed.TotalMilliseconds.ToString());
                Logger("Segundos " + stopwatch.Elapsed.TotalSeconds.ToString());
                Logger("Minutos " + stopwatch.Elapsed.TotalMinutes.ToString());
                Logger(DateTime.Now.ToString());
                Logger(" End ");
                Logger("----------------------------------------------------------------------------------");
            }
            Console.ReadLine();
        }
        static Resource FillObject(string[] aItems)
        {
            WorkZoneController workZone = new WorkZoneController();
            Resource objResource = new Resource();
            objResource.workZone = new WorkZone();

            objResource.parentId = aItems[0].Trim();
            objResource.externalId = aItems[1].Trim();
            //objResource.name = aItems[2].Trim();
            //objResource.resourceType = aItems[3].Trim();
            //objResource.resourceStatus = aItems[4];
            //objResource.language = aItems[5].Trim();
            //objResource.email = aItems[6];
            //objResource.phone = aItems[7];
            //objResource.timezone = aItems[8];
            //objResource.calendar = string.Empty;
            //objResource.time_format = aItems[9] == "24 horas" ? "24-hour" : aItems[9];
            //objResource.date_format = aItems[10] == "aaaa/mm/dd" ? "yyyy/mm/dd" : aItems[10];
            if (aItems.Count() >= 25)
            {
                if (string.IsNullOrEmpty(aItems[24]))
                    return null;
                else
                {
                    objResource.resource_workzones = aItems[25];

                    switch (aItems[24])
                    {
                        case "MX":
                            objResource.resource_workzones = aItems[25];
                            break;

                        case "CR":
                            objResource.resource_workzones = "CR" + aItems[25];
                            break;

                        case "GT":
                            objResource.resource_workzones = "GT" + aItems[25];
                            break;

                        case "HN":
                            objResource.resource_workzones = "HN" + aItems[25];
                            break;

                        case "NI":
                            objResource.resource_workzones = "NI" + aItems[25];
                            break;

                        case "PA":
                            objResource.resource_workzones = "PA" + aItems[25];
                            break;

                        case "SV":
                            objResource.resource_workzones = aItems[25];
                            break;


                        default:
                            objResource.resource_workzones = aItems[25];
                            break;
                    }

                }


            }

            else
                return null;

            //objResource.XR_PartnerID = aItems[0];

            objResource.workZone.source = objResource.resource_workzones;
            objResource.workZone.id = workZone.Ranges(objResource.workZone);
            return objResource;
        }
        private static void ReadCSV(string path)
        {
            Console.Clear();
            var files = Directory.GetFiles(path, "*.csv");

            foreach (var item in files)
            {
                try
                {
                    CSVController objCSVController = new CSVController();
                    objCSVController.source = @item;

                    Task<List<string>> task = objCSVController.LinesFile();
                    task.Wait();
                    var result = task.Result;
                    list.AddRange(result);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("Error  al leer el archivo {0} : Exepci�n :{1}", item, ex.Message));
                    Logger(string.Format("Error  al leer el archivo {0} : Exepci�n :{1}", item, ex.Message));
                }
            }
            Console.Clear();
        }

        static void Split()
        {
            listResource = new List<Resource>();
            foreach (var item in list)
            {
                CSVController objCSVController = new CSVController();
                Resource objResource = new Resource();
                string[] result = objCSVController.SplitBy(item, ';');
                objResource = FillObject(result);
                if (objResource != null)
                    listResource.Add(objResource);
            }
        }
        static string WorkZoneQueue(List<WorkZone> listworkZone)
        {
            int good = 0;
            int bad = 0;
            int limitTemp = 0;

            foreach (var item in listworkZone)
            {
                limitTemp++;
                var flag = WorkZoneMain(item);
                if (flag)
                    good++;
                else
                {
                    Logger(string.Format("workzone {0}|{1}|{2}|{3}|{4}|", item.workZoneLabel, item.status, item.travelArea, item.workZoneName, item.label.FirstOrDefault()));
                    bad++;
                }

                if (limitTemp == 1000)
                {
                    Thread.Sleep(1000);
                    limitTemp = 0;
                }
            }

            return string.Concat(listworkZone.Count, ",", good, ",", bad);
        }
        static bool WorkZoneMain(WorkZone workZone)
        {
            bool flag = false;
            WorkZoneController ctrlworkZone = new WorkZoneController();
            var checkExist = ctrlworkZone.Exist(workZone);
            if (checkExist)
                flag = ctrlworkZone.Set(workZone);
            else
                flag = ctrlworkZone.Create(workZone);

            return flag;
        }

        static void WorkZoneList(string externalId, List<string> list)
        {
            WorkZoneController workZoneController = new WorkZoneController();
            foreach (var item in list)
                workZoneController.Add(externalId, item);
        }
        public static void Logger(String lines, int opcion = 0)
        {
            string temppath = string.Empty;
            switch (opcion)
            {
                case 1:
                    temppath = @sPath + "\\log_ok.txt";
                    break;
                case 2:
                    temppath = @sPath + "\\log_not.txt";
                    break;
                case 3:
                    temppath = @sPath + "\\log_json.txt";
                    break;
                default:
                    temppath = @sPath + "\\log.txt";
                    break;
            }
            System.IO.StreamWriter file = new System.IO.StreamWriter(temppath, true);
            file.WriteLine(DateTime.Now + " :" + lines);
            file.Close();
        }

    }
}
