using SkyResources.Controllers;
using SkyResources.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Linq;
namespace SkyResources
{
    public class Program
    {
        private static ICSV icsv;
        private static IResources iresources;
        private static IUser iuser;
        private static IWorkSkill iworkskill;
        private static IUtilWebRequest iutilWebRequest;
        private static ICalendar icalendar;
 
        public static List<string> list { get; set; } = new List<string>();
        public static List<Resource> listResource { get; set; }
        public static string sPath { get; set; }
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            iutilWebRequest = new UtilWebRequest();
            iworkskill = new WorkSkillController(iutilWebRequest);
            icsv = new CSVController();
            iuser = new UserController(iutilWebRequest);
            icalendar = new CalendarController(iutilWebRequest);
            iresources = new ResourcesController(iutilWebRequest, iworkskill, iuser, icalendar);

            #region Code Load Recursos
            // Code for read recursos
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            Console.WriteLine(" Ingrese Ubicación (Folder) del archivo");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(" Por ejemplo C:\\Users\\inmotion\\Documents\\z");
            Console.ResetColor();
            // sPath = Console.ReadLine();
            sPath = @"C:\Users\inmotion\Documents\bitbucket\ofsc\skymx\doc\CargaRecursos";
            Logger(string.Format("Inicia carga de recursos {0}|{1}|| ", DateTime.Now, stopwatch.Elapsed.TotalSeconds));

            if (Directory.Exists(Program.sPath))
                Console.WriteLine("Leyendo archivos CSV");
            else
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(string.Format("Ubicación no valida {0}", Program.sPath));
                Thread.Sleep(1800);
                Console.ResetColor();
                throw new Exception(" x=> { x.id = 'error' }");
            }

            // Read csv file 
            try
            {
                string pathtmp = Program.sPath;

                var result = icsv.ReadCSV(pathtmp);
                if (result.Count > 0)
                {
                    List<Resource> listResource = icsv.SplitFillObject(result);

                    // read each item
                    // solo un calendario de trabajo 
                    listResource = listResource.OrderByDescending(x => x.resourceType == "DISP").ToList();
                    foreach (Resource item in listResource)
                    {
                        Program.Logger(string.Format("{0}|{1}|", item.externalId, item.user.login));
                        Console.Clear();
                        Console.WriteLine(string.Format("cargando external id {0}| user id {1}|", item.externalId, item.user.login));

                        try
                        {
                            bool flag = iresources.Exist(item);

                            if (flag)
                                flag = iresources.CreateReplace(item, enumMethod.PATCH);
                            else
                                flag = iresources.CreateReplace(item, enumMethod.PUT);

                            if (flag)
                                Program.Logger(string.Format("{0}|{1}|{2}|", item.XR_MasterID, item.externalId, item.user.login), 1);
                            else
                                Program.Logger(string.Format("{0}|{1}|{2}|", item.XR_MasterID, item.externalId, item.user.login), 2);
                        }
                        catch (Exception ex)
                        {
                            Program.Logger(string.Format("Recurso{0}|Error{1}|{2}|", item.externalId, ex.Message, ex.InnerException.Message), 4);
                        }
                    }
                }


                stopwatch.Stop();
                Logger(string.Format("Termina carga de recursos {0}|{1}||", DateTime.Now, stopwatch.Elapsed.TotalSeconds));
                Logger(string.Format("----------------------------------------------------------------------------------"));
                Console.Clear();
                Console.WriteLine(string.Format("El proceso tardo en leer {0} registros en {1} segundos ", result.Count, stopwatch.Elapsed.TotalSeconds));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            #endregion
            Console.ReadKey();
        }

        /// <summary>
        /// 1 log_ok.txt
        /// 2 log_not.txt
        /// 3 log_json.txt
        /// 4 log_error.txt
        /// log.txt default
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="opcion"></param>
        public static void Logger(String lines, int opcion = 0)
        {
            string temppath = string.Empty;
            try
            {
                switch (opcion)
                {
                    case 1:
                        temppath = @sPath + "\\log_ok.txt";
                        break;
                    case 2:
                        temppath = @sPath + "\\log_not.txt";
                        break;
                    case 3:
                        temppath = @sPath + "\\log_json.txt";
                        break;
                    case 4:
                        temppath = @sPath + "\\log_error.txt";
                        break;
                    default:
                        temppath = @sPath + "\\log.txt";
                        break;
                }
                System.IO.StreamWriter file = new System.IO.StreamWriter(temppath, true);
                file.WriteLine(DateTime.Now + " : " + lines);
                file.Close();
            }
            catch
            {
                Thread.Sleep(800);
                Logger(lines, opcion);

            }
        }


    }

}
