﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Models
{
    public class Resource
    {
        public string parentId { get; set; }
        public string externalId { get; set; }
        public string name { get; set; }
        public string resourceType { get; set; }
        public string resourceStatus { get; set; }
        public string language { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string timezone { get; set; }
        public string calendar { get; set; }
        //public string time_format { get; set; }

        private string _time_format;

        public string time_format
        {
            get { return _time_format; }
            set {
                switch (value)
                {
                    case "24-horas":
                        _time_format = "24-hour";
                        break;
                    case "12-horas":
                        _time_format = "12-hour";
                        break;
                    default:
                        _time_format = "24-hour";
                        break;
                }
               
            }
        }

        public string date_format { get; set; }
        public string resource_workzones { get; set; }
        public string XR_PartnerID { get; set; }
        public string XR_MasterID { get; set; }
        // public List<string> work_skills { get; set; } = new List<string>();
        public string capacity_categories { get; set; }
        public string XR_RPID { get; set; }
        public string organization_id { get; set; } = "default";
        public User user { get; set; }
        public WorkZone workZone { get; set; }
        public Calendar Calendar{ get; set; }
        public List<WorkSkill> workSkills { get; set; } = new List<WorkSkill>();

    }
}
