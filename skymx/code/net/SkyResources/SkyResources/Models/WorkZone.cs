﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Models
{
    public class WorkZone
    {
        public List<string> id { get; set; }
        public string source { get; set; }
        #region Create WorkZone
        public string workZoneName { get; set; }
        public string workZoneLabel { get; set; }
        public string travelArea { get; set; }
        public string status { get; set; } = "active";
        public List<string> label { get; set; } = new List<string>();

        #endregion
    }
}
