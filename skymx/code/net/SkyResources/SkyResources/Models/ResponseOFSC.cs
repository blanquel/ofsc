﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Models
{
    public class ResponseOFSC
    {
        public int statusCode { get; set; }
        public string Content { get; set; }

        public string type { get; set; }
        public string title { get; set; }
        public string status { get; set; }
        public string detail { get; set; }
    }
}
