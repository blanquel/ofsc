﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Models
{
    public class User
    {
        public string login { get; set; } //26//ulogin
        public string userType { get; set; } //27//user_type_id
        public string status { get; set; } //28//sustatus
        public string language { get; set; } = "es-ES"; //29//ulanguage 
        public string timezone { get; set; }//31//su_zid
        public string name { get; set; }//35//uname
        public string sutime_fid { get; set; }//9//sutime_fid
        public string sudate_fid { get; set; }//10//sudate_fid
        public string sulong_date_fid { get; set; }//XX//sulong_date_fid
        public string password { get; set; } = "Sky_TOA_2018.";//XX//password 
        public string passwordTemporary { get; set; }//XX//password_temporary
        public string mainResourceId { get; set; }//1//main_resource_id
        public string login_policy { get; set; } //1//login_policy
        public string mainResourceInternalId { get; set; }
        public string selfAssignment { get; set; }
        public string languageISO { get; set; }
        public string dateFormat { get; set; }
        public string longDateFormat { get; set; }
        public string timeFormat { get; set; }
        public string weekStart { get; set; }
        public string timeZoneDiff { get; set; }
        public string timeZone { get; set; }
        public string timeZoneIANA { get; set; }
        public string createdTime { get; set; }
        public string lastUpdatedTime { get; set; }
        public string lastPasswordChangeTime { get; set; }
    }
}
