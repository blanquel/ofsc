﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Models
{
    public class WorkSchedule
    {
        /*
          "recordType": "working",
        "startDate":  "2015-11-06",
        "endDate": "2015-11-07",
        "workTimeStart": "12:00:00",
        "workTimeEnd": "22:00:00",
        "shiftType": "regular",
        "recurrence": {
            "recurrenceType": "weekly",
            "recurEvery": 5
        }
         */
        public string recordType { get; set; }
        public string startDate { get; set; }

        public string endDate { get; set; }
        public string workTimeStart { get; set; }
        public string workTimeEnd { get; set; }
        public string shiftType { get; set; }

    }
}
