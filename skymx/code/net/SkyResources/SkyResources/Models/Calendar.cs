﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Models
{
    public class Calendar
    {
        public string recordType { get; set; } = "shift";
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string workTimeStart { get; set; }
        public string workTimeEnd { get; set; }
        public string shiftType { get; set; }
       
        // public string recurrence { get; set; }

    }
}
