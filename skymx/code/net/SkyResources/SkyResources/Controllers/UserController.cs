﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SkyResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Controllers
{
    public interface IUser
    {
        User Get(string login);
        ResponseOFSC Create(User user);
        ResponseOFSC Set(User user);
    }
    public class UserController : IUser
    {
        private IUtilWebRequest utilWebRequest;
        public UserController(IUtilWebRequest _utilWebRequest)
        {
            utilWebRequest = _utilWebRequest;
        }
        public User Get(string login)
        {
            User user = new User();
            try
            {
                var requestResource = utilWebRequest.SendWayAsync(string.Format("/rest/ofscCore/v1/users/{0}", login),
                                                                  enumMethod.GET,
                                                                  string.Empty);
                if (requestResource.statusCode >= 400)
                    return null;
                else
                {
                    user = JsonConvert.DeserializeObject<User>(requestResource.Content);
                    return user;
                }
            }
            catch (Exception ex)
            {
                Program.Logger(string.Format("user {0}|{1}|{2}|{3}|{4}|", user.login, user.name, user.status, ex.Message, ex.InnerException.Message),4);
                return null;
            }
        }

        public ResponseOFSC Create(User user)
        {
            ResponseOFSC responseOFSC = new ResponseOFSC();

            try
            {
                dynamic objUser = new JObject();
                objUser.name = user.name;
                objUser.mainResourceId = user.mainResourceId;
                objUser.language = user.language;
                objUser.timeZone = user.timeZone;
                objUser.userType = user.userType;
                objUser.password = user.password;

                // Resources add
                JArray jArray = new JArray(user.mainResourceId);
                objUser.resources = jArray;


                // create resource
                var request = utilWebRequest.SendWayAsync("rest/ofscCore/v1/users/" + user.login,
                                             enumMethod.PUT,
                                             objUser.ToString());

                responseOFSC.statusCode = request.statusCode;
                responseOFSC.Content = request.Content;

            }
            catch (Exception ex)
            {
                Program.Logger(string.Format("ResponseOFSC Create User {0}|{1}|{2}|{3}|{4}|", user.login, user.name, user.status, ex.Message, ex.InnerException.Message),4);
            }
            //return flag;
            return responseOFSC;
        }

        public ResponseOFSC Set(User user)
        {
            ResponseOFSC responseOFSC = new ResponseOFSC();
            try
            {
                dynamic objUser = new JObject();
                objUser.name = user.name;
                objUser.mainResourceId = user.mainResourceId;
                objUser.language = user.language;
                objUser.timeZone = user.timezone;
                objUser.userType = user.userType;
                objUser.password = user.password;

                // Resources add
                JArray jArray = new JArray(user.mainResourceId);
                objUser.resources = jArray;


                // create resource
                responseOFSC = utilWebRequest.SendWayAsync("rest/ofscCore/v1/users/" + user.login,
                                                          enumMethod.PATCH,
                                                          objUser.ToString(Formatting.None));
                if (responseOFSC.statusCode >= 200 && responseOFSC.statusCode < 300)
                {
                    Program.Logger(string.Format("{0}|{1}|{2}|{3}", objUser.name, objUser.mainResourceId, objUser.userType, objUser.password),1);
                }
                else
                {
                    ResponseOFSC responseError = JsonConvert.DeserializeObject<ResponseOFSC>(responseOFSC.Content);
                    Program.Logger(string.Format("type:{0}|title:{1}|status:{2}|detail:{3}", responseError.type, responseError.title, responseError.status, responseError.detail), 2);
                }

            }
            catch (Exception ex)
            {
                Program.Logger(string.Format("ResponseOFSC Set User {0}|{1}|{2}|{3}|{4}|", user.login, user.name, user.status, ex.Message, ex.InnerException.Message),4);
            }
            return responseOFSC;
        }
    }
}
