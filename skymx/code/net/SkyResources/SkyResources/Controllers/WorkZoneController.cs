﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkyResources.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SkyResources.Controllers
{
    public interface IWorkZone
    {
        Task<bool> ExistAsync(string workZone);

        bool Add(string resource, string workZone);

        List<string> Ranges(WorkZone workZone);

        bool Create(WorkZone workZone);

        bool Exist(WorkZone workZone);

        bool Set(WorkZone workZone);

    }
    public class WorkZoneController : IWorkZone
    {
        public bool Add(string resource, string workZone)
        {
            throw new NotImplementedException();
        }

        public bool Create(WorkZone workZone)
        {
            throw new NotImplementedException();
        }

        public bool Exist(WorkZone workZone)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ExistAsync(string workZone)
        {
            throw new NotImplementedException();
        }

        public List<string> Ranges(WorkZone workZone)
        {
            workZone.id = new List<string>();
            try
            {
                string[] aWorkZonetmp = workZone.source.Split('|');

                foreach (var item in aWorkZonetmp)
                {
                    if (string.IsNullOrEmpty(item))
                        continue;
                    var restmp = item.Split(',');

                    if (restmp.Count() > 1)
                    {
                        string[] aZipCode = restmp[1].Split('-');
                        if (aZipCode.Count() > 0)
                        {
                            int major = int.Parse(aZipCode[1]);
                            int minor = int.Parse(aZipCode[0]);

                            for (int i = minor; i <= major; i++)
                            {
                                string tmpzipcode = i.ToString().PadLeft(5, '0');
                                workZone.id.Add(tmpzipcode);
                            }
                        }
                    }
                    //else
                    //{
                    //    // TODO  hace falta información 
                    //}
                }
                List<string> tmpB = new List<string>();
                tmpB.AddRange(workZone.id.Distinct());
                workZone.id.Clear();
                workZone.id.AddRange(tmpB);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message + " " + ex.InnerException);
            }

            return workZone.id;

        }

        public bool Set(WorkZone workZone)
        {
            throw new NotImplementedException();
        }
    }
}
