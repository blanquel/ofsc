﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SkyResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Controllers
{
    public interface IWorkSkill
    {
        List<WorkSkill> Get(string externalId);
        ResponseOFSC Create(List<WorkSkill> list, string externalId);

    }
    public class WorkSkillController : IWorkSkill
    {
        private IUtilWebRequest utilWeb { get; set; }
        public WorkSkillController(IUtilWebRequest _utilWeb)
        {
            utilWeb = _utilWeb;
        }
        public List<WorkSkill> Get(string externalId)
        {
            List<WorkSkill> workSkills = new List<WorkSkill>();
            try
            {
                var requestResource = utilWeb.SendWayAsync(string.Format("/rest/ofscCore/v1/resources/{0}/workSkills", externalId), enumMethod.GET, string.Empty);
                if (requestResource.statusCode >= 400)
                    return null;
                else
                {
                    JObject resultado = JObject.Parse(requestResource.Content);
                    var resultadoB = resultado["items"].Children().ToList();

                    foreach (var item in resultadoB)
                    {
                        var WorkSkills = JsonConvert.DeserializeObject<WorkSkill>(item.ToString());
                        workSkills.Add(WorkSkills);
                    }
                    return workSkills;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return null;
            }
        }

        public ResponseOFSC Create(List<WorkSkill> list, string externalId)
        {
            ResponseOFSC responseOFSC = new ResponseOFSC();
            try
            {
                // add workskill resources
                JArray jArray = new JArray();
                foreach (var item in list)
                {
                    dynamic objWorkSkill = new JObject();
                    objWorkSkill.workSkill = item.workSkill;
                    objWorkSkill.ratio = item.ratio;
                    objWorkSkill.startDate = item.startDate;
                    jArray.Add(objWorkSkill);
                }

                if (jArray.Count > 0)
                {
                    var requestWorkSkills = utilWeb.SendWayAsync(string.Format("/rest/ofscCore/v1/resources/{0}/workSkills", externalId),
                                                                        enumMethod.POST,
                                                                        jArray.ToString(Formatting.None));
                    responseOFSC.statusCode = requestWorkSkills.statusCode;
                    responseOFSC.Content = requestWorkSkills.Content;
                }
            }
            catch (Exception ex)
            {
                Program.Logger(string.Format("ResponseOFSC Create {0} {1} {2}", externalId, ex.Message, ex.InnerException),4);
            }
            return responseOFSC;
        }
    }
}
