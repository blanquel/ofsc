﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SkyResources.Models;
using System;
namespace SkyResources.Controllers
{
    public interface ICalendar
    {
        ResponseOFSC Get(Resource resource);
        ResponseOFSC Create(Resource resource);
    }
    public class CalendarController : ICalendar
    {
        private IUtilWebRequest utilWebRequest;
        public CalendarController(IUtilWebRequest _utilWebRequest)
        {
            utilWebRequest = _utilWebRequest;
        }
        public ResponseOFSC Create(Resource resource)
        {
            ResponseOFSC objResponseOFSC = new ResponseOFSC();
            if (string.IsNullOrEmpty(resource.calendar))
                return null;
            try
            {
                DateTime start = DateTime.Now;
                DateTime end = start.AddDays(365);

                dynamic objResourceCalendar = new JObject();
                dynamic objResourcerecurrenceCalendar = new JObject();

                objResourcerecurrenceCalendar.recurrenceType = "daily";
                objResourcerecurrenceCalendar.recurEvery = 1;

                objResourceCalendar.recordType = "schedule"; // resource.Calendar.recordType;
                objResourceCalendar.startDate = start.ToString("yyyy-MM-dd");
                objResourceCalendar.endDate = end.ToString("yyyy-MM-dd");
                objResourceCalendar.scheduleLabel = resource.calendar;
                objResourceCalendar.comments = "Ingresado desde API";
                objResourceCalendar.shiftType = "regular";// resource.Calendar.shiftType;
                objResourceCalendar.recurrence = objResourcerecurrenceCalendar;

                Program.Logger(objResourceCalendar.ToString(Formatting.None), 3);
                Program.Logger("------------------------------------------------------");
                ResponseOFSC requestResource = utilWebRequest.SendWayAsync(
                string.Format("/rest/ofscCore/v1/resources/{0}/workSchedules", resource.externalId),
                enumMethod.POST, objResourceCalendar.ToString(Formatting.None));

                if (requestResource.statusCode >= 200 && requestResource.statusCode < 300)
                    return requestResource;
                else
                {
                    objResponseOFSC = JsonConvert.DeserializeObject<ResponseOFSC>(requestResource.Content);
                    Program.Logger(string.Format("type:{0}|title:{1}|status:{2}|detail:{3}", objResponseOFSC.type, objResponseOFSC.title, objResponseOFSC.status, objResponseOFSC.detail));
                }

            }
            catch (Exception ex)
            {
                Program.Logger(string.Format("ResponseOFSC Create {0} {1} ", ex.Message, ex.InnerException), 4);
            }

            return objResponseOFSC;
        }

        public ResponseOFSC Get(Resource resource)
        {
            throw new NotImplementedException();
        }
    }
}
