﻿using SkyResources.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Controllers
{
    public interface ICSV
    {
        List<string> ReadCSV(string path);
        Task<List<string>> LinesFileAsync();
        string[] SplitBy(string text, char character);
        List<Resource> SplitFillObject(List<string> ist);
    }
    public class CSVController : ICSV
    {
        public string source { get; set; }
        public async Task<List<string>> LinesFileAsync()
        {
            List<string> listResult = new List<string>();
            try
            {
                using (var reader = new StreamReader(this.source))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = await reader.ReadLineAsync();
                        listResult.Add(line);
                    }
                    return listResult;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Error {0}, detalles: {1}", ex.Message, ex.InnerException.Message));
            }
            return null;
        }

        public List<string> ReadCSV(string path)
        {
            List<string> list = new List<string>();
            Console.Clear();
            var files = Directory.GetFiles(path, "*.csv");

            foreach (var item in files)
            {
                try
                {
                    this.source = @item;
                    Task<List<string>> task = this.LinesFileAsync();
                    task.Wait();
                    var result = task.Result;
                    list.AddRange(result);
                }
                catch (Exception ex)
                {
                    // Console.WriteLine();
                    Program.Logger(string.Format("Error  al leer el archivo {0} : Exepción :{1}", item, ex.Message), 4);
                }
            }
            Console.Clear();
            return list;
        }

        public string[] SplitBy(string text, char character)
        {
            string[] aResult;
            try
            {
                aResult = text.Split(character);
                return aResult;
            }
            catch (Exception ex)
            {
                Program.Logger(string.Format("SplitBy Error {0} {1}", ex.Message, ex.InnerException.Message));
            }
            return null;
        }

        public List<Resource> SplitFillObject(List<string> list)
        {
            List<Resource> listResource = new List<Resource>();

            foreach (var item in list)
            {
                try
                {
                    string[] result = SplitBy(item, ';');
                    Resource objResource = FillObject(result);

                    if (objResource != null)
                        listResource.Add(objResource);
                    else
                        Program.Logger(string.Format("No se pudo completar el objecto {0}|Error|", item), 4);

                }
                catch (Exception ex)
                {
                    Program.Logger(string.Format("{0}|Error{1}|{2}|", item, ex.Message, ex.InnerException.Message), 4);
                    continue;
                }
            }
            return listResource;
        }

        public Resource FillObject(string[] aItems)
        {
            Resource objResource = new Resource();
            try
            {
                IWorkZone workZone = new WorkZoneController();
                objResource.user = new User();
                objResource.workZone = new WorkZone();

                objResource.parentId = aItems[0].Trim();
                objResource.externalId = aItems[1].Trim();
                objResource.name = aItems[2].Trim();
                objResource.resourceType = aItems[3].Trim();
                objResource.resourceStatus = aItems[4];
                objResource.language = aItems[5].Trim();
                objResource.email = aItems[6];
                objResource.phone = aItems[7];
                objResource.timezone = aItems[8];
                objResource.calendar = aItems[19];
                objResource.time_format = aItems[9] == "24 horas" ? "24-hour" : aItems[9];
                objResource.date_format = aItems[10] == "aaaa/mm/dd" ? "yyyy/mm/dd" : aItems[10];
                objResource.resource_workzones = aItems[25];
                objResource.XR_PartnerID = aItems[37];
                objResource.XR_MasterID = (aItems.Count() > 36 ? aItems[36] : string.Empty);

                // start get workskills
                var vwork_skills = aItems[17].Split('|').ToList().Where(x => !string.IsNullOrEmpty(x));
                var work_skills = vwork_skills.ToList();
                foreach (var item in work_skills)
                    objResource.workSkills.Add(new WorkSkill() { workSkill = item });
                // end get workskills

                // objResource.capacity_categories = aItems[34];
                if (aItems.Length >= 38)
                    objResource.XR_RPID = aItems[38];

                if (objResource.resourceType.ToUpper() == "DISP")
                    objResource.organization_id = string.IsNullOrEmpty(aItems[11]) ? objResource.organization_id : aItems[11];

                // fill object user
                if (aItems.Length > 26)
                {
                    objResource.user.login = aItems[26].Trim();
                    objResource.user.userType = aItems[27];
                    objResource.user.status = aItems[28];
                    objResource.user.language = aItems[29];
                    objResource.user.timezone = aItems[31];
                    objResource.user.name = aItems[35].ToString();
                    objResource.user.sutime_fid = aItems[9];
                    objResource.user.sudate_fid = aItems[10];
                    objResource.user.sulong_date_fid = "año/mes/día";
                    objResource.user.password = "Sky_TOA_2018.";
                    objResource.user.passwordTemporary = "yes";
                    if (objResource.user.userType == "TECN")
                        objResource.user.mainResourceId = aItems[1];

                    objResource.user.login_policy = "SEGSKY";
                }
                objResource.workZone.source = objResource.resource_workzones;
                objResource.workZone.id = workZone.Ranges(objResource.workZone);
                return objResource;
            }
            catch 
            {
                return null;
            }
        }
    }
}
