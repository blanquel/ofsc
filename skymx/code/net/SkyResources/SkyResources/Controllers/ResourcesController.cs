﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SkyResources.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkyResources.Controllers
{
    public interface IResources
    {

        bool Exist(Resource resource);
        bool CreateReplace(Resource resource, enumMethod enumMethod);
        //  bool CreateBackUp(Resource resource);
    }

    public class ResourcesController : IResources
    {
        private IUtilWebRequest utilWebRequest;
        private IWorkSkill workSkill;
        private IUser user;
        private ICalendar calendar;

        public ResourcesController(IUtilWebRequest _utilWebRequest,
                                   IWorkSkill _workSkill,
                                   IUser _user,
                                   ICalendar _calendar)
        {
            utilWebRequest = _utilWebRequest;
            workSkill = _workSkill;
            user = _user;
            calendar = _calendar;
        }

        public bool Exist(Resource resource)
        {
            try
            {
                var requestResource = utilWebRequest.SendWayAsync(string.Format("/rest/ofscCore/v1/resources/{0}", resource.externalId), enumMethod.GET, string.Empty);
                if (requestResource.statusCode >= 400)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return false;
            }
        }


        public bool CreateReplace(Resource resource, enumMethod enumMethod)
        {
            bool flag = false;
            try
            {
                dynamic objResource = new JObject();
                objResource.resourceId = resource.externalId;
                objResource.organization = resource.organization_id;
                objResource.email = resource.email;
                objResource.phone = resource.phone;
                objResource.status = resource.resourceStatus;
                objResource.parentResourceId = resource.parentId;
                objResource.resourceType = resource.resourceType;
                objResource.name = resource.name;
                objResource.language = resource.language;
                objResource.timeZone = resource.timezone;
                objResource.dateFormat = resource.date_format;
                objResource.timeFormat = resource.time_format;
                objResource.XR_PartnerID = resource.XR_PartnerID;
                objResource.XR_MasterID = resource.XR_MasterID;
                objResource.XR_RPID = resource.XR_RPID;

                // create resource
                Program.Logger("Recurso:" + objResource.ToString(Formatting.None), 3);
                ResponseOFSC request = utilWebRequest.SendWayAsync("rest/ofscCore/v1/resources/" + resource.externalId,
                                              enumMethod,
                                              objResource.ToString(Formatting.None));

                if (request.statusCode >= 200 && request.statusCode < 400)
                {
                    flag = true;
                    // ==================================================
                    // add workskill resources
                    List<WorkSkill> listWorkSkills = new List<WorkSkill>();
                    listWorkSkills = WorkSkillGet(resource.externalId);
                    listWorkSkills.AddRange(resource.workSkills);
                    // listWorkSkills = listWorkSkills.Where(x => x.workSkill != "ACC").ToList();
                    listWorkSkills = listWorkSkills.GroupBy(p => p.workSkill).Select(x => x.First()).ToList();

                    ResponseOFSC resultworkSkillcreater = workSkill.Create(listWorkSkills, resource.externalId);

                    if (resultworkSkillcreater.statusCode >= 400)
                    {
                        resultworkSkillcreater = JsonConvert.DeserializeObject<ResponseOFSC>(resultworkSkillcreater.Content);
                        Program.Logger(string.Format("WorkSkill Bad:externailId{4}|type:{0}|title:{1}|status:{2}|detail:{3}", resultworkSkillcreater.type, resultworkSkillcreater.title, resultworkSkillcreater.status, resultworkSkillcreater.detail, resource.externalId), 4);
                        flag = false;
                    }

                    // ==================================================
                    // check exist user 
                    User responseUser = user.Get(resource.user.login);
                    if (responseUser == null)
                    {
                        responseUser = new User();
                        responseUser.mainResourceId = resource.externalId;
                        resource.user.mainResourceId = resource.externalId;
                        resource.user.language = "es-ES";
                        resource.user.timeZone = resource.timezone;
                        var res = user.Create(resource.user);
                    }
                    else
                        user.Set(resource.user);


                    // ==================================================
                    //if (responseUser == null)
                    //    user.Create(resource.user);
                    //else
                    //    user.Set(responseUser);

                    // ==================================================
                    // add calendar work
                    calendar.Create(resource);
                }
                else
                {
                    ResponseOFSC responseError = JsonConvert.DeserializeObject<ResponseOFSC>(request.Content);
                    Program.Logger(string.Format("Recurso Bad: type:{0}|title:{1}|status:{2}|detail:{3}", responseError.type, responseError.title, responseError.status, responseError.detail), 4);
                    flag = false;
                }

            }
            catch (Exception ex)
            {
                flag = false;
                Program.Logger(string.Format("workzone {0}|{1}|{2}|{3}|{4}|", resource.externalId, resource.email, resource.organization_id, ex.Message, ex.InnerException.Message));
            }
            return flag;
        }

        public List<WorkSkill> WorkSkillGet(string externalId)
        {
            List<WorkSkill> workSkills = new List<WorkSkill>();
            try
            {
                var requestResource = utilWebRequest.SendWayAsync(string.Format("/rest/ofscCore/v1/resources/{0}/workSkills", externalId),
                                                                  enumMethod.GET,
                                                                  string.Empty);
                if (requestResource.statusCode >= 400)
                    return null;
                else
                {
                    JObject resultado = JObject.Parse(requestResource.Content);
                    var resultadoB = resultado["items"].Children().ToList();

                    foreach (var item in resultadoB)
                    {
                        var WorkSkills = JsonConvert.DeserializeObject<WorkSkill>(item.ToString());
                        workSkills.Add(WorkSkills);
                    }
                    return workSkills;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return null;
            }
        }

        public bool CreateBackUp(Resource resource)
        {
            bool flag = false;
            try
            {
                dynamic objResource = new JObject();
                objResource.resourceId = resource.externalId;
                objResource.organization = resource.organization_id;
                objResource.email = resource.email;
                objResource.phone = resource.phone;
                objResource.status = resource.resourceStatus;
                objResource.parentResourceId = resource.parentId;
                objResource.resourceType = resource.resourceType;
                objResource.name = resource.name;
                objResource.language = resource.language;
                objResource.timeZone = resource.timezone;
                objResource.dateFormat = resource.date_format;
                objResource.timeFormat = resource.time_format;
                objResource.XR_PartnerID = resource.XR_PartnerID;
                objResource.XR_MasterID = resource.XR_MasterID;
                objResource.XR_RPID = resource.XR_RPID;

                // create resource
                ResponseOFSC request = utilWebRequest.SendWayAsync("rest/ofscCore/v1/resources/" + resource.externalId,
                                                          enumMethod.PUT,
                                                          objResource.ToString(Formatting.None));

                // add workskill resources
                List<WorkSkill> listWorkSkills = new List<WorkSkill>();
                listWorkSkills = WorkSkillGet(resource.externalId);
                listWorkSkills.AddRange(resource.workSkills);
                workSkill.Create(listWorkSkills, resource.externalId);

                // ==================================================
                // check exist user 
                User responseUser = user.Get(resource.user.login);
                if (responseUser == null)
                    user.Create(resource.user);
                else
                    user.Set(resource.user);

                // ==================================================
                if (responseUser == null)
                    user.Create(resource.user);
                else
                    user.Set(responseUser);

                // ==================================================
                // add calendar work
                calendar.Create(resource);
            }
            catch (Exception ex)
            {

                throw;
            }
            return flag;
        }
    }
}
